<!DOCTYPE HTML>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.*" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.BGE_Supply_System.Controller.LoginController.AuthorizeLevel" %>
<%@page import="com.google.appengine.api.users.UserServiceFactory"%>
<% 
   if(session.getAttribute("user_id")==null){ response.sendRedirect("index");}
    
    
    Boolean addanddelete_product=session.getAttribute(AuthorizeLevel.addanddelete_product.toString()).equals("true");
    Boolean edit_cost=session.getAttribute(AuthorizeLevel.edit_cost.toString()).equals("true");
    Boolean edit_quantity=session.getAttribute(AuthorizeLevel.edit_quantity.toString()).equals("true");
    Boolean admin=session.getAttribute(AuthorizeLevel.admin.toString()).equals("true");
    Boolean user_control=session.getAttribute(AuthorizeLevel.user_control.toString()).equals("true");
    Boolean order_accept=session.getAttribute(AuthorizeLevel.order_accept.toString()).equals("true");
    Boolean sister_comapny_access=session.getAttribute(AuthorizeLevel.sister_comapny_access.toString()).equals("true");
    
    if(!sister_comapny_access){ response.sendRedirect("homePage");}
    
    %>
<html lang="en">
   <head>
      <title>BGE Supply System-Sister Companys</title>
      <meta charset="utf-8">
      
      
        <link rel="stylesheet" href="<c:url value='/css/reset.css'/>" type="text/css" media="screen">
		<link rel="stylesheet" href="<c:url value='/css/style.css'/>" type="text/css" media="screen">
		<link rel="stylesheet" href="<c:url value='/css/grid.css'/>" type="text/css" media="screen">


		<link rel="stylesheet" href="<c:url value='/css/jqx.base.css'/>" type="text/css">
        <link rel="stylesheet" href="<c:url value='/css/jqx.arctic.css'/>" type="text/css">



		<script type="text/javascript" src="<c:url value="/js/jqx/jquery-2.0.2.min.js" />"></script>   
        <script src="<c:url value='/js/jqx/jqx-all.js'/>" type="text/javascript"></script>
      
      
      
      
      <script type="text/javascript">
         var theme="arctic";
                $(document).ready(function () {
                    // prepare the data
                    var data = {};
                   
                    <% List<Entity> comapnyList = (List<Entity>) request.getAttribute("SistersCompanysList");  %>
                    
                    <% for(int x=0;x<comapnyList.size();x++){  %>
                    var row = {};
                    row["api_id"]="<%=comapnyList.get(x).getProperty("api_id")%>";
                    row["company_mail_id"]  ="<%=comapnyList.get(x).getProperty("company_mail_id")%>";
                    row["company_name"]="<%=comapnyList.get(x).getProperty("company_name")%>";
                    row["company_url"]="<%=comapnyList.get(x).getProperty("company_url")%>";
                    row["active_status"] ="<%=comapnyList.get(x).getProperty("active_status").toString().equals("1")  %>";  
                   
                    data[<%=x%>] = row;
                    <%}%>
                   
                   
         
                    var source =
                    {
                        localdata: data,
                        datatype: "array",
                        datafields:
                        [
                            
                            { name: 'api_id', type: 'string' },
                            { name: 'company_mail_id', type: 'string' },
                            { name: 'company_name', type: 'string' },
                            { name: 'company_url', type: 'string' },
                            { name: 'active_status', type: 'bool' }
                        ],
                        updaterow: function (rowid, rowdata, commit) {
                            // synchronize with the server - send update command
                            // call commit with parameter true if the synchronization with the server is successful 
                            // and with parameter false if the synchronization failder.
                            commit(true);
                        }
                    };
         		
         		
         			   
         
         
                    var dataAdapter = new $.jqx.dataAdapter(source);
                    var editrow = -1;
                    
                    $("#add").jqxButton({ theme: theme,width:'100'});
                    
                    $(".cancel").jqxButton({ theme: theme,width:'80'});
                    
                    $('.text-input').addClass('jqx-input');	
            		$('.text-input').addClass('jqx-rc-all');
            		if (theme.length > 0) {
            			$('.text-input').addClass('jqx-input-' +theme);
            			$('.text-input').addClass('jqx-widget-content-' + theme);
            			$('.text-input').addClass('jqx-rc-all-' + theme);
            		}
            		
            		
            		
            		
            		$('#new_sistercompany_form').jqxValidator({
            			rules: [
            			
            			{ input: '#company_mail_id_Input', message: 'E-mail is required!', action: 'keyup, blur', rule: 'required' },
                        { input: '#company_mail_id_Input', message: 'Invalid e-mail!', action: 'keyup', rule: 'email' },
                        { input: '#company_name_Input', message: 'Company Name is Required!', action: 'keyup, blur', rule: 'required' },
                        { input: '#company_url_Input', message: 'Company URL is Required!', action: 'keyup, blur', rule: 'required' },
                        { input: '#company_url_Input', message: 'Invalid URL !', action: 'keyup, focus', rule: function (input, commit) {
                        	
                        	var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
                        	if (regexp.test(input.val()))
                        	  {
                        	  return true;
                        	  }
                        	return false;
            			}
                        }
            			]
            		});	
            		
            		
            		
            		// validate form.
            		$("#add").click(function () {
            			var validationResult = function (isValid) {
            				if (isValid) {
            					$("#new_sistercompany_form").submit();
            				}
            			}
            			$('#new_sistercompany_form').jqxValidator('validate', validationResult);
            		});
         		
                   
         
                    // initialize jqxGrid
                    $("#jqxgrid").jqxGrid(
                    {
                        
                        source: dataAdapter,
                        pageable: true,
         			theme: theme,
         			width: '930',
         			showtoolbar: true,
                        autoheight: true,
         			sortable: true,
         			altrows: true,
         			rendertoolbar: function (toolbar) {
         			
                            var me = this;
                            var container = $("<div style='margin: 5px;'></div>");
         				
                            var button=$("<button  type='button'>New Sister Company</button>");
                            toolbar.append(container);
                            container.append(button);
                           button.jqxButton({   height: 25,theme: theme,width:'200' });
         			   
         			   button.click(function (event) {
         				   $("#addnew_popupWindow").jqxWindow('open');
                            });
                           
                        },
                        columns: [
                          { text: 'Company Name', datafield: 'company_name' },
                          { text: 'Company E-Mail', datafield: 'company_mail_id'},
                          { text: 'Company Url', datafield: 'company_url', cellsalign: 'right', cellsformat: 'c2' },
         			  { text: 'Active Status', datafield: 'active_status',cellsrenderer: function (row, column, value,columntype) {
         				  
         				  if(!value){
         					  
         					  return '<div align="center" ><button type="button" class="status_button" onclick="changestatus_btn_click(\''+data[row]["company_mail_id"]+'\')" >Active</button></div>';
         				  }else{
         					  
         					  return '<div  align="center" style="padding-top: 5px;" >Accepted </div>';
         				  }
         				 
         			    }
                         
                          }
         			
                         
                         
                        ]
                    });
         		
                    // initialize the popup window and buttons.
                    $("#confirm_Window").jqxWindow({
                        width: 250, resizable: false,  isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.20,theme: theme           
                    });
                    
                    
                    $("#addnew_popupWindow").jqxWindow({
                        width: 340,height:220, resizable: false, theme: theme, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.20 ,theme: theme          
                    });
         
                  
                 
                    $("#Cancel").jqxButton({ theme: theme,width:'80' });
                    $("#Confirm").jqxButton({ theme: theme, width:'100'});
         
                    // update the edited row when the user clicks the 'Save' button.
                    $("#Confirm").click(function () {
         		
                    	 $("#change_status_form").submit();
                    });
                });
                
                
                
                function changestatus_btn_click(a_id){
                	$("#company_mail_id").val(c_id);
                	 $("#confirm_Window").jqxWindow('open');
                }
            
      </script>
      <style type="text/css">
         .text-input
         {
         height: 25px;
         width: 200px;
         }
         .register-table
         {
         margin-top: 10px;
         margin-bottom: 10px;
         }
         .register-table td
         {
         border-spacing: 0px;
         border-collapse: collapse;
         font-family: Verdana;
         font-size: 12px;
         width: 120px;
         }
         
        
         .prompt {
         margin-top: 10px; font-size: 10px;
         }
      </style>
      <style type="text/css">
         .status_button {
         -moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
         -webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
         box-shadow:inset 0px 1px 0px 0px #ffffff;
         background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #e9e9e9));
         background:-moz-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
         background:-webkit-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
         background:-o-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
         background:-ms-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
         background:linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%);
         filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#e9e9e9',GradientType=0);
         background-color:#f9f9f9;
         -moz-border-radius:10px;
         -webkit-border-radius:10px;
         border-radius:10px;
         border:1px solid #dcdcdc;
         display:inline-block;
         cursor:pointer;
         color:#666666;
         font-family:arial;
         font-size:13px;
         font-weight:bold;
         padding:5px 37px;
         text-decoration:none;
         text-shadow:0px 3px 0px #ffffff;
         }
         .status_button:hover {
         background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #e9e9e9), color-stop(1, #f9f9f9));
         background:-moz-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
         background:-webkit-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
         background:-o-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
         background:-ms-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
         background:linear-gradient(to bottom, #e9e9e9 5%, #f9f9f9 100%);
         filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e9e9e9', endColorstr='#f9f9f9',GradientType=0);
         background-color:#e9e9e9;
         }
         .status_button:active {
         position:relative;
         top:1px;
         }
      </style>
      <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5.js"></script>
      <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
      <![endif]-->
   </head>
   <body id="page1">
      <div class="main-bg">
      <div class="bg">
         <!--==============================header=================================-->
         <header>
            <div class="main">
               <div class="wrapper">
                  <h1><a href="homePage">BGE Supply System</a></h1>
                  <div class="fright">
                     <div class="indent"> <span class="address">8901 Marmora Road, Glasgow, D04 89GR</span> <span class="phone">Tel: +1 959 552 5963</span> </div>
                  </div>
               </div>
               <nav>
                  <ul class="menu">
                     <li><a href="homePage">Home</a></li>
                     <li><a href="productPage">Products</a></li>
                     <% if(order_accept){ out.println("<li ><a href=\"ordersPage\">Orders</a></li>");} 
                        if(sister_comapny_access){ out.println("<li><a class=\"active\"  href=\"sisterComapanyPage\">Sister Companys</a></li>");} 
                        if(user_control){ out.println("<li ><a href=\"usersPage\">Companys Staffs</a></li>");}  %>
                  </ul>
               </nav>
            </div>
         </header>
         <!--==============================content================================-->
         <section id="content">
            <div class="main">
               <div class="container_12">
                  <div class="container-bot">
                     <div class="container-top">
                        <div class="container">
                           <div class="wrapper">
                              <article class="grid_8">
                                 <div id='jqxWidget'></div>
                                 <div id="jqxgrid"></div> 
                                    <div style="margin-top: 30px;">
                                       <div id="cellbegineditevent"></div>
                                       <div style="margin-top: 10px;" id="cellendeditevent"></div>
                                    </div>
                                    <div id="addnew_popupWindow">
                                       <div>Add New Sister Company</div>
                                       <div style="overflow: hidden;">
                                          <form class="form" id="new_sistercompany_form"  method="post" action="addRequestForSisterCompany" style="font-size: 13px; font-family: Verdana; width: 650px;">
                                             <table class="register-table">
                                                <tr>
                                                   <td>Company Name:</td>
                                                   <td>
                                                      <div><input name="company_name" type="text"  id="company_name_Input" placeholder="Company Name" class="text-input" /></div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>Company Email ID:</td>
                                                   <td>
                                                      <div><input name="company_mail_id" type="text"  id="company_mail_id_Input" placeholder="somecompany@mail.com" class="text-input" /></div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>Company URL:</td>
                                                   <td>
                                                      <div><input name="company_url" type="text"  id="company_url_Input" placeholder="www.somecompany.com" class="text-input" /></div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="padding-top: 10px;" align="right"></td>
                                                   <td style="padding-top: 10px;" align="right"><input style="margin-right: 5px;" type="button" id="add" value="Add" /><input id="Cancel" class="cancel" type="button" value="Cancel" /></td>
                                                </tr>
                                             </table>
                                          </form>
                                       </div>
                                    </div>
                                    <div id="confirm_Window">
                                       <div>Confirrm Dialog</div>
                                       <div style="overflow: hidden;">
                                          <form class="form" id="change_status_form"  method="post" action="requestToChangeStatusSisterCompany" style="font-size: 13px; font-family: Verdana; width: 650px;">
                                             <input type="hidden" name="company_mail_id" id="company_mail_id" value="" />
                                             <p >Are You Sure?</p>
                                             <input style="margin-left: 50px; margin-right: 10px;" type="button" id="Confirm" value="Active" /><input id="Cancel" class="cancel" type="button" value="Cancel" />
                                          </form>
                                       </div>
                                    </div>
                              </article>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </section>
         <!--==============================footer=================================-->
         <footer>
      <div class="main"><a target="_blank" href="<% out.print(UserServiceFactory.getUserService().createLogoutURL("logoutPage")); %>">Log Out</a> </div>
    </footer>
         </div>
      </div>
   </body>
</html>