<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.Collection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.google.common.collect.ArrayListMultimap" %>
<%@ page import="com.google.common.collect.Multimap" %>
<%@ page import="com.google.common.collect.ListMultimap" %>
<%@ page import="com.BGE_Supply_System.Controller.LoginController.AuthorizeLevel" %>
<%@ page import="com.BGE_Supply_System.Common.HtmlString" %>
<%@page import="com.google.appengine.api.users.UserServiceFactory"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <% if(session.getAttribute("user_id")==null)
         { 
          response.sendRedirect("index");
         
         }
         
          
          Boolean addanddelete_product=session.getAttribute(AuthorizeLevel.addanddelete_product.toString()).equals("true");
          Boolean edit_cost=session.getAttribute(AuthorizeLevel.edit_cost.toString()).equals("true");
          Boolean edit_quantity=session.getAttribute(AuthorizeLevel.edit_quantity.toString()).equals("true");
          Boolean admin=session.getAttribute(AuthorizeLevel.admin.toString()).equals("true");
          Boolean user_control=session.getAttribute(AuthorizeLevel.user_control.toString()).equals("true");
          Boolean order_accept=session.getAttribute(AuthorizeLevel.order_accept.toString()).equals("true");
          Boolean sister_comapny_access=session.getAttribute(AuthorizeLevel.sister_comapny_access.toString()).equals("true");
          
          
          
          
          List<Entity> productsList = (List<Entity>) request.getAttribute("productsList"); 
           
             String product_button_js="";
             
                 		  if(productsList.size()>0){
                 			  
                 			  product_button_js="$('.product_Button').jqxButton({ theme:theme,width: '150',height:23});";
                 		  }
          
         
         
         %>
      <title>BGE Supply System</title>
      <meta charset="utf-8">
      
        <link rel="stylesheet" href="<c:url value='/css/reset.css'/>" type="text/css" media="screen">
		<link rel="stylesheet" href="<c:url value='/css/style.css'/>" type="text/css" media="screen">
		<link rel="stylesheet" href="<c:url value='/css/grid.css'/>" type="text/css" media="screen">
      
      
      
      <link rel="stylesheet" href="<c:url value='/css/jqx.base.css'/>" type="text/css">
      <link rel="stylesheet" href="<c:url value='/css/jqx.arctic.css'/>" type="text/css">
      
      <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"
         type="text/javascript"></script>
         
         
      <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js" type="text/javascript"></script>
      
       
      <script src="<c:url value='/js/jqx/jqx-all.js'/>" type="text/javascript"></script>
     
      
      
      
      
      <script type="text/javascript">
         var theme="arctic";
         var engine_id="";
         var price="";
         $(document).ready(function() {
         
         	var icons = {
                     header: "ui-icon-circle-arrow-e",
                     activeHeader: "ui-icon-circle-arrow-s"
                 };
         	
         	$(function() {
                 $( ".accordionLayout" ).accordion({
                 	icons: icons,
                 	heightStyle: "content"
                 });
             });
         	
         	
         	
         	 // initialize the popup window and buttons.
                    
                     
                     $("#addOrder_popupWindow").jqxWindow({
                         width: 340,height:150, resizable: false, theme: theme, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.20,position:'center'           
                     });
                     
                    
                     $(".cancel").jqxButton({ theme:theme, width: '80' });
                     
                   
         			$("#order_save").jqxButton({ theme: theme,width: '100'});
         			 
         			 
         			 
         		$('.text-input').addClass('jqx-input');	
         		$('.text-input').addClass('jqx-rc-all');
         		if (theme.length > 0) {
         			$('.text-input').addClass('jqx-input-' +theme);
         			$('.text-input').addClass('jqx-widget-content-' + theme);
         			$('.text-input').addClass('jqx-rc-all-' + theme);
         		}
         		
         
         		$("#order_quantity_Input").jqxNumberInput({ width: '200', height: '25px',decimalDigits: 0,decimal: 0 , min: 0,inputMode: 'simple', spinButtons: true });
         		
         		$('#new_order_form').jqxValidator({
         			rules: [
         			{ input: '#order_quantity_Input', message: 'Order Quantity must be more the 0 !', action: 'keyup, focus', rule: function (input, commit) {
         				if (input.val()>0) {
         					return true;
         				}
         					return false;
         				}
         			}
         			]
         		});	
         		
         		// validate form.
         		$("#order_save").click(function () {
         			var validationResult = function (isValid) {
         				if (isValid) {
         					$("#new_order_form").submit();
         				}
         			}
         			$('#new_order_form').jqxValidator('validate', validationResult);
         		});
         		
         		
         		
         		
         		
         	<% out.println(addanddelete_product?HtmlString.addNewProduct_js+"\n\n"+HtmlString.deleteproduct_js+"\n\n":"");
            out.println(edit_cost?HtmlString.changecost_js+"\n\n":"");
            out.println(edit_quantity?HtmlString.changeQuantity_js+"\n\n":"");
            out.println(product_button_js);
            
            %>
         		
         		
         		
         		
         			
         	
         });
         
         function addorder_btn_click(engine_id,price){
         	$("#order_engine_id").val(engine_id);
         	$("#price").val(price);
         	 $("#addOrder_popupWindow").jqxWindow('open');
         }
         
         <% out.println(addanddelete_product?HtmlString.delete_btn_js+"\n\n":"");
            out.println(edit_cost?HtmlString.edit_cost_btn_js+"\n\n":"");
            out.println(edit_quantity?HtmlString.edit_quantity_btn_js+"\n\n":"");
            
            %>
         
         
         
      </script>
      <style type="text/css">
         .text-input
         {
         height: 25px;
         width: 200px;
         }
         .register-table
         {
         margin-top: 10px;
         margin-bottom: 10px;
         }
         .register-table td
         {
         border-spacing: 0px;
         border-collapse: collapse;
         font-family: Verdana;
         font-size: 12px;
         width: 120px;
         }
         .h2,.h3,.h5,.h4 {
         color: #1A2324;
         text-shadow: 1px 1px 1px #A39191;
         font-size: 16px;
         }
         .h3 span,.h2 span,.h4 span,.h5 span {
         color: #332EB3;
         text-shadow: -2px 2px 3px #5F9E9C;
         }
         .h2{
         font-family: Alice;
         }
         .h3{
         font-family:Almendra;
         }
         .h4{
         font-family: Almendra;
         }
         .h5{
         font-family: Macondo;
         }
         .datagrid table { border-collapse: collapse; text-align: left; width: 100%; } 
         .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #006699; -webkit-border-radius: 11px; -moz-border-radius: 11px; border-radius: 11px; }
         .datagrid table td, .datagrid table th { padding: 8px 0px; }
         .datagrid table tbody td { color: #00496B; font-weight: normal; }
         .datagrid table tbody td:first-child { border-left: none; padding-left:10px;}
         .datagrid table tbody tr:last-child td { border-bottom: none; }
         .product_text{
         font-family: Copperplate, "Copperplate Gothic Light", fantasy;
         color: #030303;
         font-size: 23;
         padding: 0;
         text-shadow: -2px 2px 3px #CCCBBC;
         }
         .product_value_text{
         font-family: Verdana, Geneva, sans-serif;
         color: #051B73;
         font-size: 23;
         padding: 0;
         text-shadow: -2px 2px 3px #CCCBBC;
         }
      </style>
      <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5.js"></script>
      <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
      <![endif]-->
   </head>
   <body id="page1">
      <div class="main-bg">
         <div class="bg">
            <!--==============================header=================================-->
            <header>
               <div class="main">
                  <div class="wrapper">
                     <h1><a href="homePage">BGE Supply System</a></h1>
                     <div class="fright">
                        <div class="indent"> <span class="address">8901 Marmora Road, Glasgow, D04 89GR</span> <span class="phone">Tel: +1 959 552 5963</span> </div>
                     </div>
                  </div>
                  <nav>
                     <ul class="menu">
                        <li><a href="homePage">Home</a></li>
                        <li><a class="active"  href="productPage">Products</a></li>
                        <% if(order_accept){ out.println("<li ><a href=\"ordersPage\">Orders</a></li>");} 
                           if(sister_comapny_access){ out.println("<li ><a href=\"sisterComapanyPage\">Sister Companys</a></li>");} 
                           if(user_control){ out.println("<li ><a href=\"usersPage\">Companys Staffs</a></li>");} 
                           
                           %>
                     </ul>
                  </nav>
               </div>
            </header>
             <!--==============================content================================-->
    <section id="content">
      <div class="main">
        <div class="container_12">
          
          <div class="container-bot">
            <div class="container-top">
              <div class="container">
               
                      <% 
                                       ListMultimap<String, Integer> fuel_multiMap = ArrayListMultimap.create();
                                       
                                       
                                       for(int x=0;x<productsList.size();x++){
                                       fuel_multiMap.put(productsList.get(x).getProperty("fuel_type").toString(),Integer.valueOf(x)); 
                                       
                                       }
                                       
                                       Set<String> f_keys = fuel_multiMap.keySet();
                                       
                                       StringBuilder product_html=new StringBuilder();
                                       
                                       product_html.append("<div style=\"margin-left: 10px;  margin-right: 10px;\" class=\"accordionLayout\">\n\t");
                                       
                                       for (String f_key : f_keys) {
                                       
                                       product_html.append("<h2  class=\"h2\">Fual Type <span style=\"margin-left: 20px;\">"+f_key+"</span></h2>\n\t<div class=\"accordionLayout\">\n\t\t");
                                       
                                       List<Integer> f_rows = fuel_multiMap.get(f_key);
                                       ListMultimap<String, Integer> cylinders_multiMap = ArrayListMultimap.create();
                                       for(int y=0;y<f_rows.size();y++){
                                        cylinders_multiMap.put(productsList.get(f_rows.get(y)).getProperty("no_of_cylinder").toString(),Integer.valueOf(f_rows.get(y))); 
                                       }
                                       
                                       Set<String> c_keys = cylinders_multiMap.keySet();
                                       
                                       for(String c_key : c_keys){
                                       
                                       product_html.append("<h3 class=\"h3\">No Of Cylinders<span style=\"margin-left: 20px;\">"+c_key+"</span></h3>\n\t\t<div class=\"accordionLayout\">\n\t\t\t");
                                       
                                       List<Integer> c_rows = cylinders_multiMap.get(c_key);
                                       			 
                                       			 ListMultimap<String, Integer> mounting_multiMap = ArrayListMultimap.create();
                                       			 for(int z=0;z<c_rows.size();z++){
                                       				 mounting_multiMap.put(productsList.get(c_rows.get(z)).getProperty("mounting_type").toString(),Integer.valueOf(c_rows.get(z))); 
                                       			 }
                                       			 
                                       			 Set<String> m_keys = mounting_multiMap.keySet();
                                        
                                        for(String m_key : m_keys){
                                       	 
                                       	 product_html.append("<h4  class=\"h4\">Mounting Type <span style=\"margin-left: 20px;\">"+m_key+"</span></h4>\n\t\t\t<div class=\"accordionLayout\">\n\t\t\t\t");
                                       	 
                                       	 List<Integer> m_rows = mounting_multiMap.get(m_key);
                                           			 
                                           			 
                                           			 for(int a=0;a<m_rows.size();a++){
                                           				 
                                           				 String engine_id=productsList.get(m_rows.get(a)).getProperty("engine_id").toString();
                                           				 String engine_cost=productsList.get(m_rows.get(a)).getProperty("engine_cost").toString();
                                           				 String quantity=productsList.get(m_rows.get(a)).getProperty("quantity").toString();
                                           				 String cubic_capacity=productsList.get(m_rows.get(a)).getProperty("cubic_capacity").toString();
                                           				 
                                           				        String add_order_html="<button onclick=\"addorder_btn_click('"+engine_id+"','"+engine_cost+"')\" class=\"product_Button\">New Order</button>",
                                           						edit_cost_html="<button onclick=\"edit_cost_btn_click('"+engine_id+"','"+ engine_cost+"')\" class=\"product_Button\">Change Cost</button>",
                                           						edit_quantity_html="<button onclick=\"edit_quantity_btn_click('"+ engine_id+"','"+quantity+"')\" class=\"product_Button\">Change Quantity</button>",
                                           						delete_product_html="<button  onclick=\"delete_btn_click('"+engine_id+"')\" class=\"product_Button\">Delete</button>";
                                           				 
                                           				 String button_1=session.getAttribute("user_role").equals("Customer") ?add_order_html:"",
                                           						 button_2=addanddelete_product?delete_product_html:"",
                                           						 button_3=edit_cost?edit_cost_html:"",
                                           						 button_4=edit_quantity?edit_quantity_html:"";
                                           				 
                                           				 
                                           				 product_html.append("<h5  class=\"h5\">Capacity<span style=\"margin-left: 20px;\">"+cubic_capacity+"</span></h5>\n\t\t\t\t<div>\n\t\t\t\t\t"+
                                           						 "<div class=\"datagrid\">\n\t<table>\n\t\t<tbody>\n\t\t\t<tr>\n\t\t\t\t<td><span class=\"product_text\">Engine Cost :"+
                                           				         "</span></td>\n\t\t\t\t<td><span class=\"product_value_text\">"+engine_cost+
                                           						 "</span></td>\n\t\t\t\t<td><span>"+button_1+
                                           						 "</span></td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td><span class=\"product_text\">Available Quantity :"+
                                           						 "</span></td>\n\t\t\t\t<td><span class=\"product_value_text\">"+quantity+
                                           						 "</span></td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td><span>"+button_3+
                                           						 "</span></td>\n\t\t\t\t<td><span>"+button_4+
                                           						 "</span></td>\n\t\t\t\t<td><span>"+button_2+
                                           						 "</span></td>\n\t\t\t</tr>\n\t\t</tbody>\n\t</table>\n</div>\n</div>\n\t\t\t"); 
                                           				 
                                           				 
                                           				 
                                           			 }
                                       			 
                                           			 product_html.append("</div>\n\t\t");
                                       			 
                                       
                                        }
                                        
                                       
                                       
                                        product_html.append("</div>\n\t\t");
                                       
                                       
                                       
                                       }
                                       
                                       product_html.append("</div>\n"); 
                                       }
                                       
                                       product_html.append("</div>\n"); 
                                       
                                       
                                       %>
                                    <div>
                                       <%  if(addanddelete_product){ out.println("<input style=\"margin-left: 15px; type=\"button\" value=\"Add New Product\" id='addnew_Button' />");} 
                                          %>
                                    </div>
                                    <%=product_html.toString() %>
                                    <% 
                                       if(addanddelete_product){
                                        out.println(HtmlString.addnew_popupWindow);
                                           out.println(HtmlString.deleteproduct_popupWindow);
                                           } 
                                          if(edit_cost){ out.print(HtmlString.changecost_popupWindow);}
                                          if(edit_quantity){ out.println(HtmlString.changeQuantity_popupWindow);} 
                                       %>
                                    <div id="addOrder_popupWindow">
                                       <div>Add New Order</div>
                                       <div style="overflow: hidden;">
                                          <form class="form" id="new_order_form"  method="post" action="addOrders" style="font-size: 13px; font-family: Verdana; width: 650px;">
                                             <input type="hidden" name="order_engine_id" id="order_engine_id" value="" />
                                             <input type="hidden" name="price" id="price" value="" />
                                             <table class="register-table">
                                                <tr>
                                                   <td>Order Quantity:</td>
                                                   <td>
                                                      <div style='margin-top: 3px;' name="quantity" id='order_quantity_Input' placeholder="Quantity" ></div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td></td>
                                                   <td style="padding-top: 10px;" align="right"><input style="margin-right: 5px;" type="button" id="order_save" value="Save" /><input id="Cancel" class="cancel" type="button" value="Cancel" /></td>
                                                </tr>
                                             </table>
                                          </form>
                                       </div>
                                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--==============================footer=================================-->
           <footer>
      <div class="main"><a target="_blank" href="<% out.print(UserServiceFactory.getUserService().createLogoutURL("logoutPage")); %>">Log Out</a> </div>
    </footer>
         </div>
      </div>
   </body>
</html>