<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <%@ page import="com.BGE_Supply_System.Controller.LoginController.AuthorizeLevel" %>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  
  <%@ page import="com.BGE_Supply_System.Controller.LoginController.AuthorizeLevel" %>
  <%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BGE Supply System</title>
</head>
<body>

<% 
 UserService userService = UserServiceFactory.getUserService();
 User user = userService.getCurrentUser();
 if (user != null) {
	 
	 if(session.getAttribute("user_id")!=null){
			
			
			
			if(session.getAttribute(AuthorizeLevel.addanddelete_product.toString())!=null && session.getAttribute(AuthorizeLevel.sister_comapny_access.toString()).equals("true")){
		    	response.sendRedirect("productPage");
		    	
		    }else if(session.getAttribute(AuthorizeLevel.order_accept.toString())!=null && session.getAttribute(AuthorizeLevel.sister_comapny_access.toString()).equals("true")){
		    	
		    	response.sendRedirect("ordersPage");
		    }else if(session.getAttribute(AuthorizeLevel.sister_comapny_access.toString())!=null && session.getAttribute(AuthorizeLevel.sister_comapny_access.toString()).equals("true")){
		    	
		    	
		    	response.sendRedirect("sisterComapanyPage");
		    }else if(session.getAttribute(AuthorizeLevel.user_control.toString())!=null && session.getAttribute(AuthorizeLevel.user_control.toString()).equals("true")){
		    	
		    	
		    	response.sendRedirect("usersPage");
		    }else{
		    	
		    	response.sendRedirect("homePage");
		    	
		    }
		    
		}else{
			
			response.sendRedirect("userCheckPage");
		}
	
 }else{
	 
	 response.sendRedirect(userService.createLoginURL("userCheckPage"));
 }



	

 %>

</body>
</html>