<!DOCTYPE HTML>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.*" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.BGE_Supply_System.Controller.LoginController.AuthorizeLevel" %>
<%@page import="com.google.appengine.api.users.UserServiceFactory"%>
<% 
   if(session.getAttribute("user_id")==null){ response.sendRedirect("index");}
    
    
    Boolean addanddelete_product=session.getAttribute(AuthorizeLevel.addanddelete_product.toString()).equals("true");
    Boolean edit_cost=session.getAttribute(AuthorizeLevel.edit_cost.toString()).equals("true");
    Boolean edit_quantity=session.getAttribute(AuthorizeLevel.edit_quantity.toString()).equals("true");
    Boolean admin=session.getAttribute(AuthorizeLevel.admin.toString()).equals("true");
    Boolean user_control=session.getAttribute(AuthorizeLevel.user_control.toString()).equals("true");
    Boolean order_accept=session.getAttribute(AuthorizeLevel.order_accept.toString()).equals("true");
    Boolean sister_comapny_access=session.getAttribute(AuthorizeLevel.sister_comapny_access.toString()).equals("true");
    
    if(!user_control){ response.sendRedirect("homePage");}
    
    %>
<html lang="en">
   <head>
      <title>BGE Supply System-User Control</title>
      <meta charset="utf-8">
      
      
        <link rel="stylesheet" href="<c:url value='/css/reset.css'/>" type="text/css" media="screen">
		<link rel="stylesheet" href="<c:url value='/css/style.css'/>" type="text/css" media="screen">
		<link rel="stylesheet" href="<c:url value='/css/grid.css'/>" type="text/css" media="screen">


		<link rel="stylesheet" href="<c:url value='/css/jqx.base.css'/>" type="text/css">
        <link rel="stylesheet" href="<c:url value='/css/jqx.arctic.css'/>" type="text/css">



		<script type="text/javascript" src="<c:url value="/js/jqx/jquery-2.0.2.min.js" />"></script>   
        <script src="<c:url value='/js/jqx/jqx-all.js'/>" type="text/javascript"></script>
      
      
      
      
      
      
      <script type="text/javascript">
         var theme="arctic";
                $(document).ready(function () {
                    // prepare the data
                    var data = {};
                   
                   
                    <% List<Entity> userList = (List<Entity>) request.getAttribute("UsersList");   %>
                    
                    <% for(int x=0;x<userList.size();x++){  %>
                    var row = {};
                    row["user_mail_id"] = "<%=userList.get(x).getProperty("user_mail_id")%>";
                    row["user_name"]="<%=userList.get(x).getProperty("user_name")%>";
                    row["add_product"]="<%=userList.get(x).getProperty("authorize_level").toString().charAt(2)=='1'%>";
                    row["order"]="<%=userList.get(x).getProperty("authorize_level").toString().charAt(3)=='1'%>";
                    row["sister"]="<%=userList.get(x).getProperty("authorize_level").toString().charAt(6)=='1'%>";
                    row["quantity"]="<%=userList.get(x).getProperty("authorize_level").toString().charAt(5)=='1'%>";
                    row["cost"]="<%=userList.get(x).getProperty("authorize_level").toString().charAt(4)=='1'%>";
                    row["user"]="<%=userList.get(x).getProperty("authorize_level").toString().charAt(1)=='1'%>";
                    row["admin"]="<%=userList.get(x).getProperty("authorize_level").toString().charAt(0)=='1'%>";
                    
                    
                    data[<%=x%>] = row;
                    <%}%>
                   
                   
         
                    var source =
                    {
                        localdata: data,
                        datatype: "array",
                        datafields:
                        [
                            { name: 'user_mail_id', type: 'string' },
                            { name: 'user_name', type: 'string' },
                            { name: 'add_product', type: 'bool' },
                            { name: 'order', type: 'bool' },
                            { name: 'sister', type: 'bool' },
                            { name: 'quantity', type: 'bool' },
                            { name: 'cost', type: 'bool' },
                            { name: 'user', type: 'bool' },
                            { name: 'admin', type: 'bool' }
                        ],
                        updaterow: function (rowid, rowdata, commit) {
                            // synchronize with the server - send update command
                            // call commit with parameter true if the synchronization with the server is successful 
                            // and with parameter false if the synchronization failder.
                            commit(true);
                        }
                    };
         		
         		
         			   
         
         
                    var dataAdapter = new $.jqx.dataAdapter(source);
                    var editrow = -1;
                    
                    $("#add").jqxButton({ theme: theme,width:'100'});	
                    $(".cancel").jqxButton({ theme: theme,width:'80'});
                    
                    $('.text-input').addClass('jqx-input');	
            		$('.text-input').addClass('jqx-rc-all');
            		if (theme.length > 0) {
            			$('.text-input').addClass('jqx-input-' +theme);
            			$('.text-input').addClass('jqx-widget-content-' + theme);
            			$('.text-input').addClass('jqx-rc-all-' + theme);
            		}
            		
            		
            		$("#password").jqxPasswordInput({  width: '200px', height: '20px', showStrength: true, showStrengthPosition: "right" });
            		
            		$('#new_user_form').jqxValidator({
            			rules: [
            			{ input: '#user_mail_id_Input', message: 'E-mail is required!', action: 'keyup, blur', rule: 'required' },
                        { input: '#user_mail_id_Input', message: 'Invalid e-mail!', action: 'keyup', rule: 'email' },
                        { input: '#user_name_Input', message: 'User Name is Required!', action: 'keyup, blur', rule: 'required' },
                        { input: '#password', message: 'Your Password is required!', action: 'keyup, blur', rule: 'required' }
            			]
            		});	
            		
            		
            		
            		// validate form.
            		$("#add").click(function () {
            			var validationResult = function (isValid) {
            				if (isValid) {
            					$("#new_user_form").submit();
            				}
            			}
            			$('#new_user_form').jqxValidator('validate', validationResult);
            		});
         		
                   
            		
            		var photorenderer = function (row, column, value) {
            			
            			if(value){
            				
            				 var imgurl = '<c:url value="/css/images/ok-icon.png" />';	
            			}else{
            				
            				 var imgurl = '<c:url value="/css/images/Error-icon.png" />';
            			}
            			
            			
                        var img = '<div style="background: white;"><img style="margin:auto;" width="20" height="20" src="'+imgurl+'"></div>';
                        return img;
                    }
         
                    // initialize jqxGrid
                    $("#jqxgrid").jqxGrid(
                    {
                        
                        source: dataAdapter,
                        pageable: true,
         			theme: theme,
         			width: '930',
         			showtoolbar: true,
                        autoheight: true,
         			sortable: true,
         			altrows: true,
         			rendertoolbar: function (toolbar) {
         			
                            var me = this;
                            var container = $("<div style='margin-top: 5px;'></div>");
         				
                            var button=$("<button  type='button'>New User</button>");
                            toolbar.append(container);
                            container.append(button);
                           button.jqxButton({   height: 25,theme: theme,width:'200' });
         			   
         			   button.click(function (event) {
         				   $("#addnew_popupWindow").jqxWindow('open');
                            });
                           
                        },
                        columns: [
                          { text: 'User Name', datafield: 'user_name', cellsalign: 'center'  },
                          { text: 'User Mail', datafield: 'user_mail_id', cellsalign: 'center'},
                          { text: 'Add/Remove Product', cellsrenderer: photorenderer,columngroup: 'authorize_level', cellsalign: 'center',datafield: 'add_product'},
                          { text: 'Order', cellsrenderer: photorenderer,columngroup: 'authorize_level', cellsalign: 'center',datafield: 'order'},
                          { text: 'Sister Companys', cellsrenderer: photorenderer,columngroup: 'authorize_level', cellsalign: 'center',datafield: 'sister'},
                          { text: 'Edit Quantity', cellsrenderer: photorenderer,columngroup: 'authorize_level', cellsalign: 'center',datafield: 'quantity'},
                          { text: 'Edit Cost', cellsrenderer: photorenderer,columngroup: 'authorize_level', cellsalign: 'center',datafield: 'cost'},
                          { text: 'User Control', cellsrenderer: photorenderer,columngroup: 'authorize_level', cellsalign: 'center',datafield: 'user'},
                          { text: 'Admin', cellsrenderer: photorenderer,columngroup: 'authorize_level', cellsalign: 'center',datafield: 'admin'}
                          
                         
                         
                        ],
                        columngroups: [
                                       { text: 'Authorize Level', align: 'center', name: 'authorize_level' }
                                   ]
                    });
                    
                    
                    
                    // create context menu
                    var contextMenu = $("#Menu").jqxMenu({ width: 200, height: 58, autoOpenPopup: false, mode: 'popup'});
                    $("#jqxgrid").on('contextmenu', function () {
                        return false;
                    });
                    // handle context menu clicks.
                    $("#Menu").on('itemclick', function (event) {
                        var args = event.args;
                        var rowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                        var dataRecord = $("#jqxgrid").jqxGrid('getrowdata', editrow);
                        if ($.trim($(args).text()) == "Delete User" && !dataRecord.admin) {
                            editrow = rowindex;
                            var offset = $("#jqxgrid").offset();
                            
                            $("#user_mail_id").val(dataRecord.user_mail_id);
                            
                            // show the popup window.
                            $("#confirm_Window").jqxWindow('show');
                        }
                        
                    });
         		
                    // initialize the popup window and buttons.
                    $("#confirm_Window").jqxWindow({
                        width: 250, resizable: false,  isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.20,theme: theme           
                    });
                    
                    
                    $("#addnew_popupWindow").jqxWindow({
                        width: 360,height:390, resizable: false, theme: theme, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.20 ,theme: theme          
                    });
         
                  
                 
                    $("#Cancel").jqxButton({ theme: theme,width:'80' });
                    $("#Confirm").jqxButton({ theme: theme, width:'100'});
         
                    // update the edited row when the user clicks the 'Save' button.
                    $("#Confirm").click(function () {
                    	
                    	
         		
                    	 $("#delete_user_form").submit();
                    });
                    
                    
                   
                    $(".jqxCheckBox").jqxCheckBox({ width: 120, height: 25,theme:theme});
                    
                    
                  
                    
                    
                    
                    
                    
                });
                
                
                
              
            
      </script>
      <style type="text/css">
         .text-input
         {
         height: 25px;
         width: 200px;
         }
         .register-table
         {
         margin-top: 10px;
         margin-bottom: 10px;
         }
         .register-table td
         {
         border-spacing: 0px;
         border-collapse: collapse;
         font-family: Verdana;
         font-size: 12px;
         width: 120px;
         }
         .prompt {
         margin-top: 10px; font-size: 10px;
         }
      </style>
      <style type="text/css">
         .status_button {
         -moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
         -webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
         box-shadow:inset 0px 1px 0px 0px #ffffff;
         background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #e9e9e9));
         background:-moz-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
         background:-webkit-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
         background:-o-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
         background:-ms-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
         background:linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%);
         filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#e9e9e9',GradientType=0);
         background-color:#f9f9f9;
         -moz-border-radius:10px;
         -webkit-border-radius:10px;
         border-radius:10px;
         border:1px solid #dcdcdc;
         display:inline-block;
         cursor:pointer;
         color:#666666;
         font-family:arial;
         font-size:13px;
         font-weight:bold;
         padding:5px 37px;
         text-decoration:none;
         text-shadow:0px 3px 0px #ffffff;
         }
         .status_button:hover {
         background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #e9e9e9), color-stop(1, #f9f9f9));
         background:-moz-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
         background:-webkit-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
         background:-o-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
         background:-ms-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
         background:linear-gradient(to bottom, #e9e9e9 5%, #f9f9f9 100%);
         filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e9e9e9', endColorstr='#f9f9f9',GradientType=0);
         background-color:#e9e9e9;
         }
         .status_button:active {
         position:relative;
         top:1px;
         }
      </style>
      <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5.js"></script>
      <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
      <![endif]-->
   </head>
   <body id="page1">
      <div class="main-bg">
         <div class="bg">
            <!--==============================header=================================-->
            <header>
               <div class="main">
                  <div class="wrapper">
                     <h1><a href="homePage">BGE Supply System</a></h1>
                     <div class="fright">
                        <div class="indent"> <span class="address">8901 Marmora Road, Glasgow, D04 89GR</span> <span class="phone">Tel: +1 959 552 5963</span> </div>
                     </div>
                  </div>
                  <nav>
                     <ul class="menu"> 
                        <li><a href="homePage">Home</a></li>
                        <li><a href="productPage">Products</a></li>
                        <% if(order_accept){ out.println("<li ><a href=\"ordersPage\">Orders</a></li>");} 
                           if(sister_comapny_access){ out.println("<li><a href=\"sisterComapanyPage\">Sister Companys</a></li>");} 
                           if(user_control){ out.println("<li><a  class=\"active\"  href=\"usersPage\">Companys Staffs</a></li>");}  %>
                     </ul>
                  </nav>
               </div>
            </header>
            <!--==============================content================================-->
            <section id="content">
               <div class="main">
                  <div class="container_12">
                     <div class="container-bot">
                        <div class="container-top">
                           <div class="container">
                              <div class="wrapper">
                                 <article class="grid_8">
                                 
								   <div id='jqxWidget'>
								   <div id="jqxgrid"></div>
								   <div style="margin-top: 30px;">
								      <div id="cellbegineditevent"></div>
								      <div style="margin-top: 10px;" id="cellendeditevent"></div>
								   </div>
								   <div id="addnew_popupWindow">
								      <div>Add New User</div>
								      <div style="overflow: hidden;">
								         <form class="form" id="new_user_form"  method="post" action="addComapanyUser" style="font-size: 13px; font-family: Verdana; width: 650px;">
								            <input type="hidden" name="own_user_mail_id" value="<%= UserServiceFactory.getUserService().getCurrentUser().getEmail().toString() %>" /> 
								            <table class="register-table">
								               <tr>
								                  <td>User Name:</td>
								                  <td style='width: 200px;' >
								                     <div><input name="user_name" type="text"  id="user_name_Input" placeholder="User Name" class="text-input" /></div>
								                  </td>
								               </tr>
								               <tr>
								                  <td>User Email ID:</td>
								                  <td style='width: 200px;'>
								                     <div><input name="user_mail_id" type="text"  id="user_mail_id_Input" placeholder="somecompany@mail.com" class="text-input" /></div>
								                  </td>
								               </tr>
								               <tr>
								                  <td>Your Password:</td>
								                  <td style='width: 200px;'>
								                     <div><input id="password" type="password" name="password" placeholder="Your Password" ></div>
								                  </td>
								               </tr>
								               <tr>
								                  <td colspan="2">
								                     <div style='float: left; width: 380px;'>
								                        <h3 style='margin-left: 15px;'>Authorize Level</h3>
								                        
								                         <div id='<%=AuthorizeLevel.order_accept.toString() %>' class='jqxCheckBox' style='margin-left: 10px; float: left;'>
								                           <span>Orders</span>
								                        </div>
								                        <div id='<%=AuthorizeLevel.addanddelete_product.toString() %>' class='jqxCheckBox' style='margin-left: 10px; float: left;'>
								                           <span>Add/Delete Product</span>
								                        </div>
								                     </div>
								                     <div style='float: left; width: 380px; margin-top: 10px;'>
								                        <div id='<%=AuthorizeLevel.edit_cost.toString() %>' class='jqxCheckBox' style='margin-left: 10px; float: left;'>
								                           <span>Edit Cost</span>
								                        </div>
								                        <div id='<%=AuthorizeLevel.edit_quantity.toString() %>' class='jqxCheckBox' style='margin-left: 10px; float: left;'>
								                           <span>Edit Quantity</span>
								                        </div>
								                     </div>
								                     <div style='float: left; width: 380px; margin-top: 10px;'>
								                        <div id='<%=AuthorizeLevel.sister_comapny_access.toString() %>' class='jqxCheckBox' style='margin-left: 10px; float: left;'>
								                           <span>Sister Company</span>
								                        </div>
								                     </div>
								                      <div style='float: left; width: 380px; margin-top: 10px;'>
								                        <% String admin_text="\n<div id='"+
								                           AuthorizeLevel.admin.toString()+"' class='jqxCheckBox' style='margin-left: 10px; float: left;'><span>Admin</div> </span><div id='"+
								                           AuthorizeLevel.user_control.toString()+"'class='jqxCheckBox' style='margin-left: 10px; float: left;'><span>User Control</span></div>\n";
								                           
								                           if(admin){ out.println(admin_text);} %> 
								                     </div>
								                  </td>
								               </tr>
								               <tr>
								                  <td colspan="2" style="padding-top: 10px; " align="right"><input style="margin-right: 20px;" type="button" id="add" value="Add" /><input style="margin-right: 40px;"  id="Cancel" class="cancel" type="button" value="Cancel" /></td>
								               </tr>
								            </table>
								         </form>
								      </div>
								   </div>
								   </div>
								   <div id="confirm_Window">
								      <div>Confirrm Dialog</div>
								      <div style="overflow: hidden;">
								         <form class="form" id="delete_user_form"  method="post" action="requestToChangeStatusSisterCompany" style="font-size: 13px; font-family: Verdana; width: 650px;">
								            <input type="hidden" name="user_mail_id" id="user_mail_id" value="" />
								            <p>Are You Sure?</p>
								            <input style="margin-left: 50px; margin-right: 10px;" type="button" id="Confirm" value="Active" /><input id="Cancel" class="cancel" type="button" value="Cancel" />
								         </form>
								      </div>
								   </div>
								<div id='Menu'>
								        <ul>
								            <li>Delete User</li>
								        </ul>
								    </div>
                                 </article>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         </section>
         <!--==============================footer=================================-->
         <footer>
      <div class="main"><a target="_blank" href="<% out.print(UserServiceFactory.getUserService().createLogoutURL("logoutPage")); %>">Log Out</a> </div>
    </footer>
      </div>
      </div>
   </body>
</html>