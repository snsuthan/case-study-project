<%@page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.Collection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<%@ page import="com.BGE_Supply_System.Controller.LoginController.AuthorizeLevel" %>
<%@ page import="com.BGE_Supply_System.Common.HtmlString" %>

<!DOCTYPE html>
<html lang="en">
<head>

<% if(session.getAttribute("user_id")==null)
 
 { 
	 response.sendRedirect("index");
 
 }
 
	 
	 Boolean addanddelete_product=session.getAttribute(AuthorizeLevel.addanddelete_product.toString()).equals("true");
	 Boolean edit_cost=session.getAttribute(AuthorizeLevel.edit_cost.toString()).equals("true");
	 Boolean edit_quantity=session.getAttribute(AuthorizeLevel.edit_quantity.toString()).equals("true");
	 Boolean admin=session.getAttribute(AuthorizeLevel.admin.toString()).equals("true");
	 Boolean user_control=session.getAttribute(AuthorizeLevel.user_control.toString()).equals("true");
	 Boolean order_accept=session.getAttribute(AuthorizeLevel.order_accept.toString()).equals("true");
	 Boolean sister_comapny_access=session.getAttribute(AuthorizeLevel.sister_comapny_access.toString()).equals("true");
	 
	 
	 

 
 %>
<title>BGE Supply System</title>
<meta charset="utf-8">


<link rel="stylesheet" href="<c:url value='/css/reset.css'/>" type="text/css" media="screen">
<link rel="stylesheet" href="<c:url value='/css/style.css'/>" type="text/css" media="screen">
<link rel="stylesheet" href="<c:url value='/css/grid.css'/>" type="text/css" media="screen">


<script src="<c:url value='/js/jquery-1.7.1.min.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/cufon-yui.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/cufon-replace.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/Vegur_500.font.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/FF-cash.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/tms-0.3.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/jquery.easing.1.3.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/tms_presets.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/jquery.equalheights.js'/>" type="text/javascript"></script>



</head>
<body id="page1">
<div class="main-bg">
  <div class="bg">
    <!--==============================header=================================-->
    <header>
      <div class="main">
        <div class="wrapper">
          <h1><a href="homePage">BGE Supply System</a></h1>
          <div class="fright">
            <div class="indent"> <span class="address">8901 Marmora Road, Glasgow, D04 89GR</span> <span class="phone">Tel: +1 959 552 5963</span> </div>
          </div>
        </div>
        <nav>
          <ul class="menu">
            <li ><a  class="active"  href="homePage">Home</a></li>
             <li><a href="productPage">Products</a></li>
         <% if(order_accept){ out.println("<li ><a href=\"ordersPage\">Orders</a></li>");} 
         if(sister_comapny_access){ out.println("<li ><a href=\"sisterComapanyPage\">Sister Companys</a></li>");} 
         if(user_control){ out.println("<li ><a href=\"usersPage\">Companys Staffs</a></li>");}  %>
          </ul>
        </nav>
        <div class="slider-wrapper">
          <div class="slider">
            <ul class="items">
            <li> <img src="<c:url value='/images/slider-img1.jpg'/>" alt="" /> </li>
            <li> <img src="<c:url value='/images/slider-img2.jpg'/>" alt="" /> </li>
            </ul>
          </div>
          <a class="prev" href="#">prev</a> <a class="next" href="#">next</a> </div>
      </div>
    </header>
    <!--==============================content================================-->
    <section id="content">
      <div class="main">
        <div class="container_12">
          <div class="wrapper p5">
            <article class="grid_4">
              <div class="wrapper">
                <figure class="img-indent"><img src="<c:url value='/images/page1-img1.png'/>" alt=""></figure>
                <div class="extra-wrap">
                  <h4>Engine Repair</h4>
                  <p class="p2">Lorem ipsum dolosit amet, consetetur sadipng elitr sed diam nonumy eirmod.</p>
                  <a class="button" href="#">Read More</a> </div>
              </div>
            </article>
            <article class="grid_4">
              <div class="wrapper">
                <figure class="img-indent"><img src="<c:url value='/images/page1-img2.png'/>" alt=""></figure>
                <div class="extra-wrap">
                  <h4>Wheel Alignment</h4>
                  <p class="p2">Tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                  <a class="button" href="#">Read More</a> </div>
              </div>
            </article>
            <article class="grid_4">
              <div class="wrapper">
                <figure class="img-indent"><img src="<c:url value='/images/page1-img3.png'/>" alt=""></figure>
                <div class="extra-wrap">
                  <h4>Fluid Exchanges</h4>
                  <p class="p2">No sea takimata sanctus est gorem ipsum dolor sit amet forem ipsum.</p>
                  <a class="button" href="#">Read More</a> </div>
              </div>
            </article>
          </div>
          <div class="container-bot">
            <div class="container-top">
              <div class="container">
                <div class="wrapper">
                  <article class="grid_8">
                    <div class="indent-left">
                      <h2>Welcome!</h2>
                      <p class="prev-indent-bot"><strong>Car Repair</strong> is one of free website templates created by TemplateMonster.com team. This website template is optimized for 1280X1024 screen resolution. It is also XHTML &amp; CSS valid. </p>
                      <p class="border-bot">This Car Repair Template goes with two packages – with PSD source files and without them. PSD source files are available for free for the registered members of TemplatesMonster.com. The basic package (without PSD source) is available for anyone without registration.</p>
                    </div>
                    <div class="wrapper">
                      <div class="grid_4 alpha">
                        <div class="indent-left">
                          <div class="maxheight indent-bot">
                            <h3>About Us</h3>
                            <p class="prev-indent-bot"><a class="link-1" href="#">Lorem ipsum dolor amet</a> conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                            <a class="link-1" href="#">Dolor amet conse ctetur</a> adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat ut enim ad. </div>
                          <a class="button" href="#">Read More</a> </div>
                      </div>
                      <div class="grid_4 omega">
                        <div class="indent-left2">
                          <div class="maxheight indent-bot">
                            <h3 class="p0">Our Services</h3>
                            <ul class="list-1">
                              <li><a href="#">Complete Computer Diagnostics</a></li>
                              <li><a href="#">Complete Safety Analysis</a></li>
                              <li><a href="#">Drivability Problems</a></li>
                              <li><a href="#">Oil Changes</a></li>
                              <li><a href="#">Emission Repair Facility</a></li>
                              <li><a href="#">Air Conditioning Service</a></li>
                              <li><a href="#">Electrical Systems</a></li>
                              <li><a href="#">Fleet Maintenance</a></li>
                            </ul>
                          </div>
                          <a class="button" href="#">Read More</a> </div>
                      </div>
                    </div>
                  </article>
                  <article class="grid_4">
                    <div class="indent-left2 indent-top">
                      <div class="box p4">
                        <div class="padding">
                          <div class="wrapper">
                            <figure class="img-indent"><img src="<c:url value='/images/page1-img4.png'/>" alt=""></figure>
                            <div class="extra-wrap">
                              <h3 class="p0">Our Hours:</h3>
                            </div>
                          </div>
                          <p class="p1"><strong>24 Hour Emergency Towing</strong></p>
                          <p class="color-1 p0">Monday - Friday: 7:30 am - 6:00</p>
                          <p class="color-1 p1">Saturday: 7:30 am - Noon</p>
                          Night Drop Available </div>
                      </div>
                      <figure class="indent-bot">
                       
                      </figure>
                      <div class="indent-left">
                        <dl class="main-address">
                          <dt>Demolink.org 8901 Marmora Road,<br>
                            Glasgow, D04 89GR.</dt>
                          <dd><span>Telephone:</span> +1 959 552 5963;</dd>
                          <dd><span>FAX:</span> +1 959 552 5963</dd>
                          <dd><span>E-mail:</span><a href="#">mail@demolink.org</a></dd>
                        </dl>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--==============================footer=================================-->
    <footer>
      <div class="main"><a target="_blank" href="<% out.print(UserServiceFactory.getUserService().createLogoutURL("logoutPage")); %>">Log Out</a> </div>
    </footer>
  </div>
</div>
<script type="text/javascript">
$(window).load(function () {
    $('.slider')._TMS({
        duration: 1000,
        easing: 'easeOutQuint',
        preset: 'simpleFade',
        slideshow: 5000,
        banners: false,
        pauseOnHover: true,
        pagination: false,
        pagNums: false,
        nextBu: '.next',
        prevBu: '.prev'
    });
});
</script>
</body>
</html>