<!DOCTYPE HTML>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.*" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.BGE_Supply_System.Controller.LoginController.AuthorizeLevel" %>
<%@page import="com.google.appengine.api.users.UserServiceFactory"%>


<% 
if(session.getAttribute("user_id")==null){ response.sendRedirect("index");}
 
 
 Boolean addanddelete_product=session.getAttribute(AuthorizeLevel.addanddelete_product.toString()).equals("true");
 Boolean edit_cost=session.getAttribute(AuthorizeLevel.edit_cost.toString()).equals("true");
 Boolean edit_quantity=session.getAttribute(AuthorizeLevel.edit_quantity.toString()).equals("true");
 Boolean admin=session.getAttribute(AuthorizeLevel.admin.toString()).equals("true");
 Boolean user_control=session.getAttribute(AuthorizeLevel.user_control.toString()).equals("true");
 Boolean order_accept=session.getAttribute(AuthorizeLevel.order_accept.toString()).equals("true");
 Boolean sister_comapny_access=session.getAttribute(AuthorizeLevel.sister_comapny_access.toString()).equals("true");
 
 if(!order_accept){ response.sendRedirect("homePage");}
 
 %>
<html lang="en">
<head>
<title>BGE Supply System-Orders</title>
<meta charset="utf-8">


 		<link rel="stylesheet" href="<c:url value='/css/reset.css'/>" type="text/css" media="screen">
		<link rel="stylesheet" href="<c:url value='/css/style.css'/>" type="text/css" media="screen">
		<link rel="stylesheet" href="<c:url value='/css/grid.css'/>" type="text/css" media="screen">


		<link rel="stylesheet" href="<c:url value='/css/jqx.base.css'/>" type="text/css">
        <link rel="stylesheet" href="<c:url value='/css/jqx.arctic.css'/>" type="text/css">



		<script type="text/javascript" src="<c:url value="/js/jqx/jquery-2.0.2.min.js" />"></script>   
        <script src="<c:url value='/js/jqx/jqx-all.js'/>" type="text/javascript"></script>





<script type="text/javascript">
    
	var theme="arctic";
        $(document).ready(function () {
            // prepare the data
            var orders_data = {};
            var request_data = {};
            <% List<Entity> ordersList = (List<Entity>) request.getAttribute("ordersList");
            List<Entity> requestList = (List<Entity>) request.getAttribute("requestList");
            
            %>
            
            <% for(int x=0;x<ordersList.size();x++){ %>
            var o_row = {};
            o_row["customer_name"] = "<%=ordersList.get(x).getProperty("customer_name")%>";
            o_row["customer_email_id"] = "<%=ordersList.get(x).getProperty("customer_email_id")%>";
            o_row["order_id"]="<%=ordersList.get(x).getProperty("order_id")%>";
            o_row["engine_id"]  ="<%=ordersList.get(x).getProperty("engine_id")%>";
            o_row["price"]="<%=ordersList.get(x).getProperty("price")%>";
            o_row["quanity"]="<%=ordersList.get(x).getProperty("quantity")%>";
            o_row["order_type"] ="<%=ordersList.get(x).getProperty("order_type").toString().equals("0")  %>";  //0-Customer Order 1-Company Request order
            o_row["order_status"] ="<%=ordersList.get(x).getProperty("order_status").toString().equals("0") %>";  //0-Not Accept 1-Order Accept
            orders_data[<%=x%>] = o_row;
            <%}%>
            
            
            <% for(int y=0;y<requestList.size();y++){ %>
            var r_row = {};
            r_row["order_id"]="<%=ordersList.get(y).getProperty("order_id")%>";
            r_row["company_mail_id"]  ="<%=ordersList.get(y).getProperty("company_mail_id")%>";
            r_row["company_name"]  ="<%=ordersList.get(y).getProperty("company_name")%>";
            r_row["order_quantity"]="<%=ordersList.get(y).getProperty("order_quantity")%>";
            r_row["price"]="<%=ordersList.get(y).getProperty("price")%>";
            r_row["request_status"] ="<%=ordersList.get(y).getProperty("request_status").toString().equals("0")  %>";  
            request_data[<%=y%>] = r_row;
            <%}%>
           
           

            var orders_source =
            {
                localdata: orders_data,
                datatype: "array",
                datafields:
                [
                 
  					{ name: 'email_id', type: 'string' },
                    { name: 'customer_name', type: 'string' },
                    { name: 'order_id', type: 'string' },
                    { name: 'engine_id', type: 'string' },
                    { name: 'price', type: 'string' },
                    { name: 'quanity', type: 'number' },
                    { name: 'order_type', type: 'bool' },
					{ name: 'order_status', type: 'bool' }
                ],
                updaterow: function (rowid, rowdata, commit) {
                    // synchronize with the server - send update command
                    // call commit with parameter true if the synchronization with the server is successful 
                    // and with parameter false if the synchronization failder.
                    commit(true);
                },
                id :'order_id'
            };
            
            
            
            var request_source =
            {
                localdata: request_data,
                datatype: "array",
                datafields:
                [
					{ name: 'order_id', type: 'string' },
                    { name: 'company_name', type: 'string' },
                    { name: 'company_mail_id', type: 'string' },
                    { name: 'order_quantity', type: 'string' },
                    { name: 'price', type: 'string' },
                    { name: 'request_status', type: 'bool' }
                ]
                
            };
			
			
				   


            var orders_dataAdapter = new $.jqx.dataAdapter(orders_source);
            var editrow = -1;
            
            
            
            var requestDataAdapter = new $.jqx.dataAdapter(request_source, { autoBind: true });
            request = requestDataAdapter.records;
            var nestedGrids = new Array();
            
         // create nested grid.
            var initrowdetails = function (index, parentElement, gridElement, record) {
                var id = record.uid.toString();
                var grid = $($(parentElement).children()[0]);
                nestedGrids[index] = grid;
                var filtergroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = id;
                var filtercondition = 'equal';
                var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // fill the orders depending on the id.
                var requestbyid = [];
                for (var m = 0; m < request.length; m++) {
                    var result = filter.evaluate(request[m]["order_id"]);
                    if (result)
                    	requestbyid.push(request[m]);
                }
            
                var request_source =
                {
                    
                    datafields:
                    [
                    { name: 'order_id', type: 'string' },
                    { name: 'company_name', type: 'string' },
                    { name: 'company_mail_id', type: 'string' },
                    { name: 'order_quantity', type: 'string' },
                    { name: 'price', type: 'string' },
                    { name: 'request_status', type: 'bool' }
                    ],
                    id: 'order_id',
                    localdata: requestbyid
                    
                };
                
                var nestedGridAdapter = new $.jqx.dataAdapter(request_source); 
                
                if (grid != null) {
                    grid.jqxGrid({
                    	theme: theme,
                    	autoheight: true,
                        source: nestedGridAdapter,
                        columns: [
                          { text: 'Order ID', datafield: 'order_id', width: 200 , cellsalign: 'center'},
                          { text: 'Company Name', datafield: 'company_name', width: 200 , cellsalign: 'center'},
                          { text: 'Order Quantity', datafield: 'order_quantity', width: 150, cellsalign: 'center' },
                          { text: 'Price', datafield: 'price', width: 150 , cellsalign: 'center'},
                          { text: 'Request Status', datafield: 'request_status', width: 200 , cellsalign: 'center'}
                       ]
                    });
                }
            }
         
            var renderer = function (row, column, value) {
                return '<span style="margin-left: 4px; margin-top: 9px; float: left;">' + value + '</span>';
            }
         
         

            // initialize jqxGrid
            $("#jqxgrid").jqxGrid(
            {
                
                source: orders_dataAdapter,
                pageable: true,
				theme: theme,
				width: 930,
                autoheight: true,
                rowdetails: true,
                sortable: true,
                filterable: true,
                rowsheight: 28,
                initrowdetails: initrowdetails,
                rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 2px;'></div>", rowdetailsheight:100, rowdetailshidden: true },
                
                columns: [
                  { text: 'Customer Name', datafield: 'customer_name',cellsalign: 'center', cellsrenderer: renderer},
                  { text: 'Order ID', datafield: 'order_id', cellsalign: 'center',cellsrenderer: renderer},
                  { text: 'Product ID', datafield: 'engine_id' , cellsalign: 'center',cellsrenderer: renderer},
                  { text: 'Unit Price', datafield: 'price', cellsalign: 'center', cellsrenderer: renderer },
                  { text: 'Quanity', datafield: 'quanity', cellsalign: 'center', cellsformat: 'c2' , cellsrenderer: renderer},
				  { text: 'Order Status', datafield: 'order_status',cellsalign: 'center',cellsrenderer: function (row, column, value,columntype) {
					  
					  if(value){
						  
						  return '<div align="center" ><button type="button" class="status_button" style="margin-left: 4px; margin-top: 1px; float: left;" onclick="changestatus_btn_click(\''
								  +orders_data[row]["order_id"]+'\',\''+orders_data[row]["order_type"]+'\',\''+orders_data[row]["customer_email_id"]+'\',\''+orders_data[row]["engine_id"]+'\')" >Active</button></div>';
					  }else{
						  
						  return '<div align="center">Accepted</div>';
					  }
					 
				    }
                 
                  }
				
                 
                 
                ]
            });
			
            // initialize the popup window and buttons.
            $("#active_popupWindow").jqxWindow({
                width: 320,height:120, resizable: false,  isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.20 , theme: theme         
            });
            
            
            
            $("#sister_compy_request_status").jqxCheckBox({ width: 120, height: 25});
           
          
         
            $("#Cancel").jqxButton({ theme: theme,width:'80'});
            $("#Save").jqxButton({ theme: theme,width: '100' });

            // update the edited row when the user clicks the 'Save' button.
            $("#Save").click(function () {
                 $("#active_popupWindow").jqxWindow('hide');
                 $("#order_active_form").submit();
            });
        });
        
        
        function changestatus_btn_click(order_id,order_type,email_id,engine_id){
        	var temp_order_type="";
        	
        	if(order_type){
        		temp_order_type="Customer";  // Customer
        	}else{
        		temp_order_type="Company";  // Company
        	}
        	$("#order_id").val(order_id);
        	$("#order_type").val(temp_order_type);
        	$("#customer_email_id").val(email_id);
        	$("#engine_id").val(engine_id);
        	$("#active_popupWindow").jqxWindow('open');
        }
    </script>
    
   
    <style type="text/css">
    
     .text-input
         {
         height: 25px;
         width: 200px;
         }
         .register-table
         {
         margin-top: 10px;
         margin-bottom: 10px;
         }
         .register-table td
         {
         border-spacing: 0px;
         border-collapse: collapse;
         font-family: Verdana;
         font-size: 12px;
         width: 120px;
         }
        
       .status_button {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #e9e9e9));
	background:-moz-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
	background:-webkit-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
	background:-o-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
	background:-ms-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
	background:linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#e9e9e9',GradientType=0);
	background-color:#f9f9f9;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	cursor:pointer;
	color:#666666;
	font-family:arial;
	font-size:13px;
	font-weight:bold;
	padding:3px 30px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.status_button:hover {

	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #e9e9e9), color-stop(1, #f9f9f9));
	background:-moz-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
	background:-webkit-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
	background:-o-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
	background:-ms-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
	background:linear-gradient(to bottom, #e9e9e9 5%, #f9f9f9 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e9e9e9', endColorstr='#f9f9f9',GradientType=0);
	background-color:#e9e9e9;
}
.status_button:active {

	position:relative;
	top:1px;
}
    </style>





<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page1">
<div class="main-bg">
  <div class="bg">
    <!--==============================header=================================-->
    <header>
      <div class="main">
        <div class="wrapper">
          <h1><a href="homePage">BGE Supply System</a></h1>
          <div class="fright">
            <div class="indent"> <span class="address">8901 Marmora Road, Glasgow, D04 89GR</span> <span class="phone">Tel: +1 959 552 5963</span> </div>
          </div>
        </div>
        <nav>
          <ul class="menu">
            <li><a href="homePage">Home</a></li>
             <li><a href="productPage">Products</a></li>
         <% if(order_accept){ out.println("<li><a  class=\"active\" href=\"ordersPage\">Orders</a></li>");} 
         if(sister_comapny_access){ out.println("<li ><a href=\"sisterComapanyPage\">Sister Companys</a></li>");} 
         if(user_control){ out.println("<li ><a href=\"usersPage\">Companys Staffs</a></li>");}  %>
          </ul>
        </nav>
       
      </div>
    </header>
    <!--==============================content================================-->
    <section id="content">
      <div class="main">
        <div class="container_12">
          
          <div class="container-bot">
            <div class="container-top">
              <div class="container">
                <div class="wrapper">
                  <article class="grid_8">
				  
				  
				  <div id='jqxWidget'>
        <div id="jqxgrid"></div>
        <div style="margin-top: 30px;">
            <div id="cellbegineditevent"></div>
            <div style="margin-top: 10px;" id="cellendeditevent"></div>
       </div>
       <div id="active_popupWindow">
            <div>Active Order</div>
            <div style="overflow: hidden;">
                <form class="form" id="order_active_form"  method="post" action="acceptOrder" style="font-size: 13px; font-family: Verdana; width: 650px;">
             <input type="hidden" name="order_id" id="order_id" value="" />
             <input type="hidden" name="order_type" id="order_type" value="" />
            <input type="hidden" name="customer_email_id" id="customer_email_id" value="" />
			<input type="hidden" name="engine_id" id="engine_id" value="" />
			<div id='sister_compy_request_status' style='margin-left: 10px;  margin-top:10px;  float: left;'>
                Request to Sister Company to orders<p style="margin-left: 50px; margin-top:30px; margin-right: 10px;"><input style=" margin-right: 10px;" type="button" id="Save" value="Active" /><input id="Cancel" class="cancel" type="button" value="Cancel" /></p></div>
	       
           </form>
            </div>
          </div>  
    </div>
                    
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--==============================footer=================================-->
    <footer>
      <div class="main"><a target="_blank" href="<% out.print(UserServiceFactory.getUserService().createLogoutURL("logoutPage")); %>">Log Out</a> </div>
    </footer>
  </div>
</div>
</body>
</html>