package com.BGE_Supply_System.Controller;


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;


@Controller
public class LoginController {
	
	@RequestMapping({ "/","/**/index"})
	public String getAddCustomerPage(ModelMap model) {
		
		return "index";

	}
	
	
	@RequestMapping({"/**/homePage"})
	public String getHomePage(ModelMap model) {
		
		 UserService userService = UserServiceFactory.getUserService();
	        User user = userService.getCurrentUser();
	        if (user != null) {
	        	return "home";
	        }else{
	        	
	        	return "redirect:index";
	        }
		
		

	}
	
	
	@RequestMapping({"/**/logoutPage"})
	public String getLogoutPage(ModelMap model,HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		session.invalidate();
		
		return "redirect:"+UserServiceFactory.getUserService().createLoginURL("userCheckPage");
	     

	}
	
	
	
	
	
	
	
	
	@RequestMapping({"/**/userCheckPage"})
	public String getuserCheckPage(ModelMap model,HttpServletRequest request) {
		
		
		 UserService userService = UserServiceFactory.getUserService();
	        User user = userService.getCurrentUser();
	        if (user != null) {
	        	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	        	
	    		Query total_query = new Query("Users").addSort("user_name", Query.SortDirection.ASCENDING);
	    	
	    		Filter a_Filter=new FilterPredicate("user_mail_id",FilterOperator.EQUAL, user.getEmail());
	    		Query query = new Query("Users");
	    		query.setFilter(a_Filter);
	    		PreparedQuery pq = datastore.prepare(query);
	    		if(pq.countEntities(FetchOptions.Builder.withDefaults())>0){
	    			Entity company_user = pq.asSingleEntity();
	    			
	    			
	    			if(company_user.getProperty("user_id")==null){
	    				
	    				company_user.setProperty("user_id", user.getUserId());
	    				datastore.put(company_user);
	    			}
	    			
	    			HttpSession session = request.getSession();
	                session.setAttribute("user_id",company_user.getProperty("user_id"));
	                session.setAttribute("user_name",company_user.getProperty("user_name"));
	                session.setAttribute("user_role","CompanyStaff");
	                String authorize_level=company_user.getProperty("authorize_level").toString();
	                
	                for (int i = 0; i < authorize_level.length(); i++) {
	                	
	                	session.setAttribute(AuthorizeLevel.values()[i].toString(),String.valueOf(authorize_level.charAt(i)).equals("1")?"true":"false");
	    				
	    			}
	                
	                
	                //setting session to expiry in 30 mins
	                session.setMaxInactiveInterval(30*60);
	              
	                
	                
	    		}else{
	    			
	    			HttpSession session = request.getSession();
	                session.setAttribute("user_id",user.getUserId());
	                session.setAttribute("user_name",user.getNickname());
	                
	                if(datastore.prepare(total_query).asList(FetchOptions.Builder.withDefaults()).isEmpty()){
	                	
	                    session.setAttribute("user_role","Admin");
	                    
	                    for (int i = 0; i < 7; i++) {
	                    	
	                    	session.setAttribute(AuthorizeLevel.values()[i].toString(),"true");
	                    	
	                    	
	        				
	        			}
	                    createAdmin(user.getUserId(), user.getNickname(), user.getEmail());
	                    
	        			
	        		}else{
	        			
	        			session.setAttribute("user_role","Customer");
	                    
	                    for (int i = 0; i < 7; i++) {
	                    	
	                    	session.setAttribute(AuthorizeLevel.values()[i].toString(),"false");
	        				
	        			}
	        			
	        			
	        		}
	                
	                
	                
	    			
	    			
	    			
	    		}
	        	
	        	
	        }

			
		    
	        return "redirect:index";

		

	}
	
private void createAdmin(String user_id,String user_name,String user_mail_id){
		
		Key userKey = KeyFactory.createKey("Users",user_id);
    	
        String password=Long.toHexString(Double.doubleToLongBits(Math.random()));
		
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        
        Entity user = new Entity("Users", userKey);
        user.setProperty("user_id", user_id);
        user.setProperty("user_name", user_name);
        user.setProperty("password", password);
        user.setProperty("authorize_level", "1111111");
        user.setProperty("user_mail_id", user_mail_id);

       
        datastore.put(user);
        
        
        String msgBody="User Name :\t"+user_name+"\n"+
        "User ID :\t"+user_id+"\n"+
        "Password :\t"+password+"\n Your Authorize Level :Admin\n\n"+"Please Change the Password in after first Login";
        
        sendUserCreateMail(user_mail_id,msgBody);
		
		
	}
	
	
	
	
	
	
	
	
	public void sendUserCreateMail(String mail_id,String msgBody){
		
		 Properties props = new Properties();
	        Session session = Session.getDefaultInstance(props, null);
	        
	       

	        try {
	            Message msg = new MimeMessage(session);
	            msg.setFrom(new InternetAddress("admin@BGESupplySystem.com"));
	            msg.addRecipient(Message.RecipientType.TO,
	                             new InternetAddress(mail_id));
	            msg.setSubject("Your BGE User account has been Created");
	            msg.setText(msgBody);
	            Transport.send(msg);
	            
	            
	            

	        } catch (AddressException e) {
	            // ...
	        } catch (MessagingException e) {
	            // ...
	        }
	}
	
	
	
	public enum AuthorizeLevel{
		admin,
		user_control,
		addanddelete_product,
		order_accept,
		edit_cost,
		edit_quantity,
		sister_comapny_access
		
		
	}
	
	public boolean isValidEmailAddress(String email) {
	       java.util.regex.Pattern p = java.util.regex.Pattern.compile(".+@.+\\.[a-z]+");
	       java.util.regex.Matcher m = p.matcher(email);
	       return m.matches();
	}
	
	
	
	

}


