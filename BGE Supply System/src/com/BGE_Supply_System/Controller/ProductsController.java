package com.BGE_Supply_System.Controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;

@Controller
public class ProductsController {
	
	@RequestMapping(value="/**/productPage", method = RequestMethod.GET)
	public String productPage(ModelMap model) {

		return "redirect:products";

	}
	
	
	
	@RequestMapping(value="/**/addProduct", method = RequestMethod.POST)
	public ModelAndView addProduct(HttpServletRequest request, ModelMap model) {

		String engine_id = request.getParameter("engine_id");
		String cubic_capacity = request.getParameter("cubic_capacity");
		String fuel_type = request.getParameter("fuel_type");
		String no_of_cylinder = request.getParameter("no_of_cylinder");
		String mounting_type = request.getParameter("mounting_type");
		String engine_cost = request.getParameter("engine_cost");
		String quantity = request.getParameter("quantity");
		
	    Key productKey = KeyFactory.createKey("Products", engine_id);
	        
		Date date = new Date();
        Entity product = new Entity("Products", productKey);
        product.setProperty("engine_id", engine_id);
        product.setProperty("cubic_capacity", cubic_capacity);
        product.setProperty("fuel_type", fuel_type);
        product.setProperty("no_of_cylinder", no_of_cylinder);
        product.setProperty("mounting_type", mounting_type);
        product.setProperty("quantity", quantity);
        product.setProperty("engine_cost", engine_cost);
        product.setProperty("last_cost_change", date);
        product.setProperty("last_quantity_change", date);

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(product);
        
        return new ModelAndView("redirect:products");
        
	}
		
	
	
	@RequestMapping(value="/**/updateCost", method = RequestMethod.POST)
	public ModelAndView updateCost(HttpServletRequest request, ModelMap model) {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		 
		String engine_cost = request.getParameter("engine_cost");
		String engine_id = request.getParameter("cost_engine_id");
		
		Filter c_Filter =new FilterPredicate("engine_id",FilterOperator.EQUAL, engine_id);
		Date date = new Date();
		Query query = new Query("Products");
		query.setFilter(c_Filter);
		PreparedQuery pq = datastore.prepare(query);
		Entity product = pq.asSingleEntity();
		product.setProperty("engine_cost", engine_cost);
	    product.setProperty("last_cost_change", date);
	    datastore.put(product);

	    
				
        
        //return to list
        return new ModelAndView("redirect:products");
        
	}
	
	
	@RequestMapping(value="/**/updateQuantity", method = RequestMethod.POST)
	public ModelAndView updateQuantity(HttpServletRequest request, ModelMap model) {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		 
		String quantity = request.getParameter("quantity");
		String engine_id = request.getParameter("quantity_engine_id");
		
		Filter c_Filter =new FilterPredicate("engine_id",FilterOperator.EQUAL, engine_id);
		Date date = new Date();
		Query query = new Query("Products");
		query.setFilter(c_Filter);
		PreparedQuery pq = datastore.prepare(query);
		Entity product = pq.asSingleEntity();
		product.setProperty("quantity", quantity);
	    product.setProperty("last_quantity_change", date);
	    datastore.put(product);
				
        
        //return to list
        return new ModelAndView("redirect:products");
        
	}
		
	@RequestMapping(value="/**/productDelete", method = RequestMethod.POST)
	public ModelAndView productDelete(HttpServletRequest request, ModelMap model) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        String engine_id = request.getParameter("delete_engine_id");
        Filter c_Filter =new FilterPredicate("engine_id",FilterOperator.EQUAL, engine_id);
		Query query = new Query("Products");
		query.setFilter(c_Filter);
		PreparedQuery pq = datastore.prepare(query);
		Entity product = pq.asSingleEntity();
		datastore.delete(product.getKey());
		
		
		
		
        
        //return to list
        return new ModelAndView("redirect:products");
        
	}
	
	
	//get all customers
	@RequestMapping(value="/**/products", method = RequestMethod.GET)
	public String products(ModelMap model) {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query query = new Query("Products").addSort("engine_id", Query.SortDirection.ASCENDING);
	    List<Entity> products = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
	    
	    model.addAttribute("productsList",  products);
	   
	    
		return "products";

	}
	
	
	
	
	
}
