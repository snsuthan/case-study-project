package com.BGE_Supply_System.Controller;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.BGE_Supply_System.Controller.LoginController.AuthorizeLevel;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;


@Controller
public class UsersController {
	
	@RequestMapping(value="/**/usersPage", method = RequestMethod.GET)
	public String getAddCustomerPage(ModelMap model) {

		return "redirect:user";

	}
	
	
	@RequestMapping(value="/**/user", method = RequestMethod.GET)
	public String listOrders(ModelMap model) {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		Query query = new Query("Users").addSort("user_name", Query.SortDirection.ASCENDING);
	    List<Entity> user = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
	   
	    
	    model.addAttribute("UsersList",  user);
	    
		return "user";

	}
	
	
	
	@RequestMapping(value="/**/addComapanyUser", method = RequestMethod.POST)
	public ModelAndView addOrders(HttpServletRequest request, ModelMap model) {

		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Filter u_Filter =new FilterPredicate("user_mail_id",FilterOperator.EQUAL, request.getParameter("own_user_mail_id"));
		Filter p_Filter =new FilterPredicate("password",FilterOperator.EQUAL, request.getParameter("password"));
		Filter query_Filter = Query.CompositeFilterOperator.and(u_Filter, p_Filter);
		Query query = new Query("Users");
		
		query.setFilter(query_Filter);
		PreparedQuery pq = datastore.prepare(query);
		StringBuilder authorize_level = new StringBuilder("0000000");
		String user_auth_level="";
		for (int j = 0; j < authorize_level.length(); j++) {
			
			if(request.getParameter(AuthorizeLevel.values()[j].toString())!=null && request.getParameter(AuthorizeLevel.values()[j].toString()).equals("true")){
				 authorize_level.setCharAt(j, '1');
				 user_auth_level+=(AuthorizeLevel.values()[j].toString()+" ");
			 }
			
		}
		
        
        if(pq.countEntities(FetchOptions.Builder.withDefaults())>0){
			
        	
        	Key userKey = KeyFactory.createKey("Users",request.getParameter("user_mail_id"));
        	
	        String password=Long.toHexString(Double.doubleToLongBits(Math.random()));
    		
            Entity user = new Entity("Users", userKey);
            user.setProperty("user_mail_id", request.getParameter("user_mail_id"));
            user.setProperty("user_name", request.getParameter("user_name"));
            user.setProperty("password", password);
            user.setProperty("authorize_level", authorize_level.toString());

           
            datastore.put(user);
            
            
            String msgBody="User Name :\t"+request.getParameter("user_name")+"\n"+
            "Password :\t"+password+"\n Your Authorize Level :"+user_auth_level+"\n\n"+"Please Change the Password in after first Login";
            
            sendUserCreateMail(request.getParameter("user_mail_id"),msgBody);
        	
        }
		
		
	    
        
        return new ModelAndView("redirect:user");
        
	}
	
	
	
	public void sendUserCreateMail(String mail_id,String msgBody){
		
		 Properties props = new Properties();
	        Session session = Session.getDefaultInstance(props, null);
	        
	       

	        try {
	            Message msg = new MimeMessage(session);
	            msg.setFrom(new InternetAddress("admin@BGESupplySystem.com", "Admin"));
	            msg.addRecipient(Message.RecipientType.TO,
	                             new InternetAddress(mail_id));
	            msg.setSubject("Your BGE User account has been Created");
	            msg.setText(msgBody);
	            Transport.send(msg);
	            
	            
	            

	        } catch (AddressException e) {
	            // ...
	        } catch (MessagingException e) {
	            // ...
	        } catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	
	

}
