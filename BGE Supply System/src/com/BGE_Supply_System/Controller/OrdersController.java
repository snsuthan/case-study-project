package com.BGE_Supply_System.Controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import flexjson.*;

import com.BGE_Supply_System.Common.JSONParser;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
@Controller
public class OrdersController {
	
	JSONParser jsonParser = new JSONParser();
	
	
	String own_company_mail_id;
	
	String message;
	
	
	
	@RequestMapping(value="/**/ordersPage", method = RequestMethod.GET)
	public String getAddCustomerPage(ModelMap model) {

		return "redirect:orders";

	}
	
	
	//get all orders
	@RequestMapping(value="/**/orders", method = RequestMethod.GET)
	public String listOrders(ModelMap model) {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query query = new Query("Orders").addSort("order_time", Query.SortDirection.ASCENDING);
	    List<Entity> orders = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
	    
	    query = new Query("RequestOrders").addSort("order_id", Query.SortDirection.ASCENDING);
	    List<Entity> request_orders = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
	   
	    
	    model.addAttribute("ordersList",  orders);
	    model.addAttribute("requestList",  request_orders);
	   
	    
		return "orders";

	}
	
	
	
	
	
	
	@RequestMapping(value="/**/addOrders", method = RequestMethod.POST)
	public ModelAndView addOrders(HttpServletRequest request, ModelMap model) {

		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		User user = UserServiceFactory.getUserService().getCurrentUser();
		
		Query query = new Query("Orders").addSort("order_id", Query.SortDirection.DESCENDING);
	    List<Entity> orders = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(1));
	    
	    long order_id=0;
	    
	    if(orders.size()>0){
	    	order_id=((long) (orders.get(0).getProperty("order_id")))+1;
	    }
		
	    Key productKey = KeyFactory.createKey("Orders",own_company_mail_id+order_id);
	        
		Date date = new Date();
        Entity order = new Entity("Orders", productKey);
        order.setProperty("order_id", order_id);
        order.setProperty("engine_id", request.getParameter("order_engine_id"));
        order.setProperty("customer_name",user.getNickname());
        order.setProperty("customer_email_id", user.getEmail());
        order.setProperty("quantity", request.getParameter("quantity"));
        order.setProperty("price", request.getParameter("price"));
        order.setProperty("order_status",0);
        order.setProperty("order_type","Customer");
        order.setProperty("order_time", date);

       
        datastore.put(order);
        
        return new ModelAndView("redirect:products");
        
	}
	
	
	
	
	@RequestMapping(value="/**/acceptOrder", method = RequestMethod.POST)
	public ModelAndView acceptOrder(HttpServletRequest request, ModelMap model) {

		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		String order_id = request.getParameter("order_id");
		String order_type =request.getParameter("order_type") ;
		String customer_email_id =request.getParameter("customer_email_id") ;
		String sister_compy_request_status = request.getParameter("sister_compy_request_status");
		String engine_id = request.getParameter("engine_id");
		
		
		
		Filter query_Filter ;
		
		
		query_Filter =new FilterPredicate("engine_id",FilterOperator.EQUAL, engine_id);
		Query q = new Query("Products").setFilter(query_Filter);
		PreparedQuery pq = datastore.prepare(q);
		Entity product = pq.asSingleEntity();
		int available_quantity = Integer.parseInt(product.getProperty("quantity").toString());
		
		
		
		Filter a_Filter =new FilterPredicate("order_id",FilterOperator.EQUAL, order_id);
		Filter c_Filter =new FilterPredicate("customer_email_id",FilterOperator.EQUAL, customer_email_id);
		Filter o_Filter =new FilterPredicate("order_type",FilterOperator.EQUAL, order_type);
		query_Filter = Query.CompositeFilterOperator.and(c_Filter, a_Filter, o_Filter);
		Query query = new Query("Orders").setFilter(query_Filter);
		PreparedQuery o_pq = datastore.prepare(query);
		Entity order = o_pq.asSingleEntity();
		String price = order.getProperty("price").toString();
		int quantity =  Integer.parseInt(order.getProperty("quantity").toString());
		
		
		
		
		if(sister_compy_request_status.equals("true")){
			
			
			
			if(available_quantity>=quantity){
				
				createOrderRequest(order_id,own_company_mail_id,quantity,1,price); // 1- order active status
				available_quantity-=quantity;
			}else{
				
				createOrderRequest(order_id,own_company_mail_id,available_quantity,1,price);
				available_quantity=0;
			}
			
			
			
			
			if(order_type.equals("Company")){
				
				requestStatusChangeCall(customer_email_id,order_id,quantity,engine_id);
			}else{
				// E-mail Send to customer
		    	// change Order order_status
				changeOrderStatus(order_id,quantity-available_quantity,Integer.parseInt(price));
				
			}
			updateProductQuantity(available_quantity,engine_id);
			
			
		}else{
			
            if(available_quantity>=quantity){
				
				createOrderRequest(order_id,own_company_mail_id,quantity,1,price);
				available_quantity-=quantity;
				
				
				
				
				if(order_type.equals("Company")){
					
					requestStatusChangeCall(customer_email_id,order_id,quantity,engine_id);
					
					
				}else{
					changeOrderStatus(order_id,quantity,Integer.parseInt(price));
					
				}
				
				
				updateProductQuantity(available_quantity,engine_id);
				
				
			}else if(!order_type.equals("Company")){
				
				if(available_quantity>0){
					createOrderRequest(order_id,own_company_mail_id,available_quantity,1,price);
					updateProductQuantity(0,engine_id);
					quantity-=available_quantity;
				}
				
				
				
				List<Entity> companylist=getSisterCompanyDetails();
				
				for(Entity company :companylist){
					
					String url=company.getProperty("company_url").toString()+"/requestOrderCreate";
					
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair("company_mail_id",own_company_mail_id ));
					params.add(new BasicNameValuePair("api_id", company.getProperty("api_id").toString()));
					params.add(new BasicNameValuePair("order_id", order_id));
					params.add(new BasicNameValuePair("engine_id", engine_id));
					params.add(new BasicNameValuePair("quantity", quantity+""));
					
					JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);
					
					
					int success = 0;
					try {
						success = Integer.parseInt(json.getString("success"));
						
						if(success==1){
							
							int ordered_quantity=Integer.parseInt(json.getString("quantity"));
							quantity-=ordered_quantity;
							createOrderRequest(order_id,company.getProperty("company_mail_id").toString(),ordered_quantity,0,json.getString("price"));	
							if(quantity<1){
								
								break;
							}
							
							
						}	
						
						
						
						
						
					} catch (NumberFormatException | JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					
				}
				
				
				
				
			}else{
				
				
				
			}
			
		
			
			
		}
		
		
		
        
        return new ModelAndView("redirect:orders");
        
	}
	
	
	
	private void createOrderRequest(String order_id,String company_mail_id,int order_quantity,int request_status,String price){
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		Key productKey = KeyFactory.createKey("RequestOrders",company_mail_id+order_id);
		
        Entity RequestOrders = new Entity("RequestOrders", productKey);
        RequestOrders.setProperty("order_id", order_id);
        RequestOrders.setProperty("company_mail_id", company_mail_id);
        RequestOrders.setProperty("order_quantity", order_quantity);
        RequestOrders.setProperty("request_status",request_status);
        RequestOrders.setProperty("price",price);
        
        datastore.put(RequestOrders);
		
		
	}
	
	
	private List<Entity> getSisterCompanyDetails(){
		
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		
		Query query = new Query("SistersCompanys").addSort("company_mail_id", Query.SortDirection.ASCENDING);
	    List<Entity> company = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
		
		
		return company;
		
		
	}
	
	
	@RequestMapping(value="/**/requestOrderCreate", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> getAvailableQuantity(HttpServletRequest request, ModelMap model,HttpServletResponse resp ) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        
        String company_mail_id = request.getParameter("company_mail_id");
        String api_id = request.getParameter("api_id");
        String order_id = request.getParameter("order_id");
        String engine_id = request.getParameter("engine_id");
        int quantity = Integer.parseInt(request.getParameter("quantity")) ;
        
        Boolean success=false;
        HashMap<String, String> response_result = new HashMap<String, String>();
        if(checkCompanyApi_Id(api_id,company_mail_id)){
        	
        	
        	Filter c_Filter =new FilterPredicate("engine_id",FilterOperator.EQUAL, engine_id);
    		Query query = new Query("Products");
    		query.setFilter(c_Filter);
    		PreparedQuery pq = datastore.prepare(query);
    		Entity result = pq.asSingleEntity();
    		int available_quantity = (int) result.getProperty("quantity");
    		
    		if(available_quantity>0){
    			
    			int temp;
    			
    			if(available_quantity>quantity){
    				temp=quantity;
    				
    			}else{
    				
    				temp=available_quantity;
    			}
    			
    			
    			
    			Filter sis_Filter =new FilterPredicate("api_id",FilterOperator.EQUAL, api_id);
    			Query sis_query = new Query("SistersCompanys");
    			sis_query.setFilter(sis_Filter);
    			PreparedQuery sis_pq = datastore.prepare(sis_query);
    			Entity sis_cmpy = sis_pq.asSingleEntity();
    			
    			Filter price_Filter =new FilterPredicate("engine_id",FilterOperator.EQUAL, engine_id);
    			Query price_query = new Query("Products");
    			price_query.setFilter(price_Filter);
    			PreparedQuery price_pq = datastore.prepare(sis_query);
    			Entity price_cmpy = price_pq.asSingleEntity();
    			
    		    Key productKey = KeyFactory.createKey("Orders",company_mail_id+order_id);
    		        
    			Date date = new Date();
    	        Entity order = new Entity("Orders", productKey);
    	        order.setProperty("order_id", order_id);
    	        order.setProperty("engine_id", engine_id);
    	        order.setProperty("customer_name", sis_cmpy.getProperty("company_name"));
    	        order.setProperty("customer_email_id", sis_cmpy.getProperty("company_mail_id"));
    	        order.setProperty("quantity", temp);
    	        order.setProperty("price",price_cmpy.getProperty("price"));
    	        order.setProperty("order_status",0);
    	        order.setProperty("order_type","Company");
    	        order.setProperty("order_time", date);

    	        datastore.put(order);
    	        
    	        response_result.put("price",(String) price_cmpy.getProperty("price"));
    	        response_result.put("quantity",temp+"");
    			
    	        success=true;
    			
    		}
        	
        	
        }
        
        response_result.put("success",success?"1":"0");
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
 
    	JSONSerializer serializer = new JSONSerializer();
        
        return new ResponseEntity<String>(serializer.serialize(response_result), headers, HttpStatus.OK);	
		
        
	}
	
	private void updateProductQuantity(int available_quantity,String engine_id){
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Filter c_Filter =new FilterPredicate("engine_id",FilterOperator.EQUAL, engine_id);
		Date date = new Date();
		Query query = new Query("Products");
		query.setFilter(c_Filter);
		PreparedQuery pq = datastore.prepare(query);
		Entity product = pq.asSingleEntity();
		product.setProperty("quantity", available_quantity);
	    product.setProperty("last_quantity_change", date);
	    datastore.put(product);
		
		
	}
	
	
	private Boolean checkCompanyApi_Id(String api_id,String company_mail_id){
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Filter a_Filter =new FilterPredicate("api_id",FilterOperator.EQUAL, api_id);
		Filter c_Filter =new FilterPredicate("company_mail_id",FilterOperator.EQUAL, company_mail_id);
		Filter s_Filter =new FilterPredicate("active_status",FilterOperator.EQUAL, 1);
		Filter query_Filter = Query.CompositeFilterOperator.and(c_Filter, a_Filter,s_Filter);
		Query query = new Query("SistersCompanys");
		query.setFilter(query_Filter);
		PreparedQuery pq = datastore.prepare(query);
		if(pq.countEntities(FetchOptions.Builder.withDefaults())>0){
		return true;	
		}
		
		return false;
		
		
	}
	    
	
	
	
	@RequestMapping(value="**/changeRequestOrderStatus", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> changeRequestOrderStatus(HttpServletRequest request, ModelMap model,HttpServletResponse resp ) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        
        String company_mail_id = request.getParameter("company_mail_id");
        String api_id = request.getParameter("api_id");
        String order_id = request.getParameter("order_id");
        
        Boolean success=false;
        HashMap<String, String> response_result = new HashMap<String, String>();
        if(checkCompanyApi_Id(api_id,company_mail_id)){
        	
        	
        	Filter o_Filter =new FilterPredicate("order_id",FilterOperator.EQUAL, order_id);
        	Filter c_Filter =new FilterPredicate("company_mail_id",FilterOperator.EQUAL, company_mail_id);
        	Filter query_Filter = Query.CompositeFilterOperator.and(c_Filter, o_Filter);
    		Query query = new Query("RequestOrders");
    		query.setFilter(query_Filter);
    		PreparedQuery pq = datastore.prepare(query);
    		Entity order = pq.asSingleEntity();
    		order.setProperty("request_status", 1);
    	    datastore.put(order);
    	    
    	    
    		query.setFilter(o_Filter);
    		pq = datastore.prepare(query);
    		
    		int total_price=0,quantity=0;
    		
    		for (Entity result : pq.asIterable()) {
    			if(result.getProperty("request_status").equals("1")){
    				success=true;
    				total_price+=(Long.parseLong(result.getProperty("price").toString())*Long.parseLong(result.getProperty("quantity").toString()));
    				quantity+=Long.parseLong(result.getProperty("quantity").toString());
    				
    			    }else{
    			    success=false;	
    			    break;	
    			    }
    			}
    	    
    	    if(success){
    	    	
    	    	// E-mail Send to customer
    	    	// change Order order_status
    	    	changeOrderStatus(order_id,quantity,total_price);
    	    //	sendOrderConfirmMail(order_id);
    	    	
    	    	
    	    }
    	    
    	    
    		
    		
        }
        
        response_result.put("success",success?"1":"0");
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
 
    	JSONSerializer serializer = new JSONSerializer();
        
        return new ResponseEntity<String>(serializer.serialize(response_result), headers, HttpStatus.OK);	
		
        
	}
	
	
	public void changeOrderStatus(String order_id,int quantity,int total_price){
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Filter a_Filter =new FilterPredicate("order_id",FilterOperator.EQUAL, order_id);
		Query query = new Query("Orders");
		query.setFilter(a_Filter);
		PreparedQuery pq = datastore.prepare(query);
		Entity order = pq.asSingleEntity();
		
		order.setProperty("order_status",1);
	    datastore.put(order);
	    
	    
	    String massage="You have ordered for our BGE SUPPLY SYSTEM \nThe Engine TYPE :"+order.getProperty("engine_id").toString()
	    		+"\nNumber of Engine You Ordered:"+order.getProperty("quantity").toString()+"\nAccepted Order Qunatity :"+quantity+"\nThe total Price of Engines:"+total_price;
	    
	   sendMail(massage, order.getProperty("customer_email_id").toString(),order.getProperty("customer_name").toString());
		
	}
	
	public void requestStatusChangeCall(String company_mail_id,String order_id,int available_quantity,String engine_id){
		
		
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		Filter a_Filter =new FilterPredicate("company_mail_id",FilterOperator.EQUAL, company_mail_id);
		Query query = new Query("SistersCompanys");
		query.setFilter(a_Filter);
		PreparedQuery pq = datastore.prepare(query);
		Entity company = pq.asSingleEntity();
		String api_id=company.getProperty("api_id").toString();
		String url=company.getProperty("company_url").toString()+"/changeRequestOrderStatus";
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("company_mail_id",own_company_mail_id ));
		params.add(new BasicNameValuePair("api_id", api_id));
		params.add(new BasicNameValuePair("order_id", order_id));
		
		JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);
		
		int result = 0;
		try {
			result = Integer.parseInt(json.getString("success"));
			
			if(result==1){
			}	
			
			
			
			
			
		} catch (NumberFormatException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	public void sendMail(String message,String email_id,String name){
		
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		

		try {
		    Message msg = new MimeMessage(session);
		    msg.setFrom(new InternetAddress("admin@bgesupplysystem.com", "Admin"));
		    msg.addRecipient(Message.RecipientType.TO,
		     new InternetAddress(email_id, name));
		    msg.setSubject("Your Engine Order is Accepted");
		    msg.setText(message);
		    Transport.send(msg);

		} catch (AddressException e) {
		    // ...
		} catch (MessagingException e) {
		    // ...
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	

	
}
