package com.BGE_Supply_System.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.BGE_Supply_System.Common.JSONParser;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

import flexjson.JSONSerializer;


@Controller
public class SisterCompanyController {
	
	
String own_company_mail_id="info@bge.com",own_company_url="http://bgesupplysystem.appspot.com",own_company_name="BGE Supply System";
	
	
	JSONParser jsonParser = new JSONParser();
	
	
	
	@RequestMapping(value="/**/sisterComapanyPage", method = RequestMethod.GET)
	public String sisterComapanyPage(ModelMap model) {

		return "redirect:sistercompany";

	}
	
	@RequestMapping(value="/**/addAsSisterCompany", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> addAsSisterCompany(HttpServletRequest request, ModelMap model,HttpServletResponse resp ) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
      
        
        HashMap<String, String> response_result = new HashMap<String, String>();
        
        
        Key companyKey = KeyFactory.createKey("SistersCompanys",request.getParameter("company_mail_id"));
        
        Entity company = new Entity("SistersCompanys", companyKey);
        company.setProperty("api_id", request.getParameter("api_id"));
        company.setProperty("company_name", request.getParameter("company_name"));
        company.setProperty("company_mail_id", request.getParameter("company_mail_id"));
        company.setProperty("company_url", request.getParameter("company_url"));
        company.setProperty("active_status", 0);
        
        datastore.put(company);
       
        response_result.put("success","1");
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
 
    	JSONSerializer serializer = new JSONSerializer();
        
        return new ResponseEntity<String>(serializer.serialize(response_result), headers, HttpStatus.OK);	
		
        
	}
	
	
	
	
	@RequestMapping(value="/**/changeCompanyActiveStatus", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> changeCompanyActiveStatus(HttpServletRequest request, ModelMap model,HttpServletResponse resp ) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
      
        
        HashMap<String, String> response_result = new HashMap<String, String>();
        Boolean success=false;
        
		
		Filter query_Filter =new FilterPredicate("company_mail_id",FilterOperator.EQUAL,request.getParameter("company_mail_id"));
		Query query = new Query("SistersCompanys");
		query.setFilter(query_Filter);
		PreparedQuery pq = datastore.prepare(query);
        
        if(pq.countEntities(FetchOptions.Builder.withDefaults())>0){
			Entity sis_cmpy = pq.asSingleEntity();
			sis_cmpy.setProperty("active_status",1);
		    datastore.put(sis_cmpy);
		    success=true;
        }
        
       
        response_result.put("success",success?"1":"0");
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
 
    	JSONSerializer serializer = new JSONSerializer();
        
        return new ResponseEntity<String>(serializer.serialize(response_result), headers, HttpStatus.OK);	
		
        
	}
	
	
	
	@RequestMapping(value="/**/addRequestForSisterCompany", method = RequestMethod.POST)
	public ModelAndView addRequestForSisterCompany(HttpServletRequest request, ModelMap model) {

		
		String company_name=request.getParameter("company_name");
		String company_mail_id=request.getParameter("company_mail_id");
		String company_url=request.getParameter("company_url");
		
		
		String api_id = UUID.randomUUID().toString();
		
		String url=company_url+"/addAsSisterCompany";
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("company_name",own_company_name));
		params.add(new BasicNameValuePair("api_id", api_id));
		params.add(new BasicNameValuePair("company_mail_id", own_company_mail_id));
		params.add(new BasicNameValuePair("company_url",own_company_url));
		JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);
		
		
		int success = 0;
		try {
			success = Integer.parseInt(json.getString("success"));
			
			if(success==1){
				success = 0;
				url=own_company_url+"/addAsSisterCompany";
				
				params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("company_name",company_name));
				params.add(new BasicNameValuePair("api_id", api_id));
				params.add(new BasicNameValuePair("company_mail_id", company_mail_id));
				params.add(new BasicNameValuePair("company_url",company_url));
				json = jsonParser.makeHttpRequest(url, "POST", params);
				
				success = Integer.parseInt(json.getString("success"));
				if(success==1){
					
					
					
				}
				
			}	
			
			
			
			
			
		} catch (NumberFormatException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
        
        return new ModelAndView("redirect:sistercompany");
        
	}
	
	
	
	
	@RequestMapping(value="/**/requestToChangeStatusSisterCompany", method = RequestMethod.POST)
	public ModelAndView requestToChangeStatusSisterCompany(HttpServletRequest request, ModelMap model) {
		
		String company_url=request.getParameter("company_url");
		String api_id=request.getParameter("api_id");
		String company_mail_id=request.getParameter("company_mail_id");
		
		
		
		
		String url=company_url+"/changeCompanyActiveStatus";
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("company_mail_id",own_company_mail_id ));
		params.add(new BasicNameValuePair("api_id", api_id));
		
		JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);
		
		
		int success = 0;
		try {
			success = Integer.parseInt(json.getString("success"));
			
			if(success==1){
				success = 0;
				url=own_company_url+"/changeCompanyActiveStatus";
				
				params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("company_mail_id",company_mail_id ));
				params.add(new BasicNameValuePair("api_id", api_id));
				json = jsonParser.makeHttpRequest(url, "POST", params);
				
				success = Integer.parseInt(json.getString("success"));
				if(success==1){
					
					
					
				}
				
			}	
			
			
			
			
			
		} catch (NumberFormatException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
        return new ModelAndView("redirect:sistercompany");
        
	}
	
	
	@RequestMapping(value="/**/sistercompany", method = RequestMethod.GET)
	public String sistercompany(ModelMap model) {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query query = new Query("SistersCompanys").addSort("company_name", Query.SortDirection.ASCENDING);
	    List<Entity> company = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
	    
	    model.addAttribute("SistersCompanysList",  company);
	   
	    
		return "sistercompany";

	}
	
	
	
	
}
