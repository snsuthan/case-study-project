package com.BGE_Supply_System.Common;

public class HtmlString {
	
	public static String deleteproduct_js= "\n $(\"#deleteproduct_popupWindow\").jqxWindow({\n width: 300,height:100, resizable: false,"+
			" theme: theme, isModal: true, autoOpen: false, cancelButton: $(\"#Cancel\"),"+
			" modalOpacity: 0.20,position:\'center\' \n });\n\t\t\t $(\"#delete\").jqxButton({ theme: theme,width: \'100\'});\n\t\t\n\t\t$(\"#delete\")."+
			"click(function () {\n\t\t\t\n\t\t\t\n\t\t\t\t\t$(\"#delete_product_form\").submit();\n\t\t\t\n\t\t});\n\t\t";


	public static  String addNewProduct_js="$(\"#addnew_Button\").jqxButton({ width: \'150\',theme:theme});\n\n\t\n\t$(\"#addnew_Button\").on(\'click\', function () {\n "+
			"$(\"#addnew_popupWindow\").jqxWindow({ position:\'center\'});\n $(\"#addnew_popupWindow\").jqxWindow(\'open\');\t\t\t\t \n "+
			"});\n\t\t\t\t\n\t\t\t\t$(\"#addnew_popupWindow\").jqxWindow({\n width: 340,height:310, resizable: false, theme: theme, isModal: true, autoOpen: false, "+
			"cancelButton: $(\"#Cancel\"), modalOpacity: 0.20 \n });\n\t\t\t\n\t\t\t\n\t\t\t $(\"#Save\").jqxButton({ theme: theme,width: \'100\'});\t\n\t\t\t \n\t\t\t $(\"#no_of_cylinder_Input\").jqxNumberInput({ width: \'200\', height: \'25px\',decimalDigits: 0,decimal: 0 , min: 0,inputMode: \'simple\', spinButtons: true"+
			" });\n\t\t\n\t\t$(\"#cubic_capacity_Input\").jqxNumberInput({ width: \'200\', height: \'25px\',decimalDigits: 2,decimal: 0 , min: 0,inputMode: \'simple\', "+
			"spinButtons: true });\n\t\t\n\t\t$(\"#engine_cost_Input\").jqxNumberInput({ width: \'200\', height: \'25px\',decimalDigits: 2,decimal: 0 , min: 0,inputMode:"+
			" \'simple\', spinButtons: true,spinMode: \'simple\',spinButtonsStep:1,groupSize: 3, groupSeparator: \',\',symbol: \'Rs "+
			"\'});\n\t\t\n\t\t$(\"#quantity_Input\").jqxNumberInput({ width: \'200\', height: \'25px\',decimalDigits: 0,decimal: 0 , min: 0,inputMode: \'simple\', spinButtons:"+
			" true });\n\t\t\n\t\t\n\t\tvar "+
			"mounting_type_source = [\n \"Front-wheel drive\",\n \"Rear-wheel drive\"\n\t\t ];\n // Create a jqxDropDownList\n "+
			"$(\"#mounting_type_List\").jqxDropDownList({ source: mounting_type_source,theme: theme,selectedIndex: 1, dropDownHeight: \'50\',width: \'200\',"+
			" height: \'25\'});\n\t\t\t\t\n\t\t\t\tvar fuel_type_source = [\n \"Gasoline\",\n \"Diesel\",\"Hydrogen\",\"Compressed Natural Gas\",\"Electric Batteries\"\n\t\t ];\n"+
			" // Create a jqxDropDownList\n $(\"#fuel_type_List\").jqxDropDownList({ source: fuel_type_source,theme: theme,selectedIndex: 1, dropDownHeight:"+
			"\'50\',width: \'200\', height: \'25\',autoDropDownHeight: true});\n\t\t\t\t\n\t\t\t\t\n\t\t\t\t\n\t\t\t\t // initialize "+
			"validator.\n\t\t$(\'#addnew_form\').jqxValidator({\n\t\t\trules: [\n\t\t\t{ input: \'#engine_id_Input\', message: \'Engine Id is required!\', action: \'keyup, blur\',"+
			" rule: \'required\' },\n\t\t\t{ input: \'#cubic_capacity_Input\', message: \'Cubic Capacity must be more the 0 !\', action: \'keyup, focus\', rule: function "+
			"(input, commit) {\n\t\t\t\t// call commit with false, when you are doing server validation and you want to display a validation error on this field. \n\t\t\t\tif "+
			"(input.val()>0) {\n\t\t\t\t\treturn true;\n\t\t\t\t}\n\t\t\t\t\treturn false;\n\t\t\t\t}\n\t\t\t},\n\t\t\t{ input: \'#quantity_Input\', message: \'Number of engine must be more "+
			"the 0 !\', action: \'keyup, focus\', rule: function (input, commit) {\n\t\t\t\t// call commit with false, when you are doing server validation and you want to"+
			" display a validation error on this field. \n\t\t\t\tif (input.val()>0) {\n\t\t\t\t\treturn true;\n\t\t\t\t}\n\t\t\t\t\treturn false;\n\t\t\t\t}\n\t\t\t},{ input: "+
			"\'#engine_cost_Input\', message: \'Engine Cost is required!\', action: \'keyup, focus\', rule: function (input, commit) {\n\t\t\t\t// call commit with false, "+
			"when you are doing server validation and you want to display a validation error on this field. \n\t\t\t\tif (input.val()>0) {\n\t\t\t\t\treturn "+
			"true;\n\t\t\t\t}\n\t\t\t\t\treturn false;\n\t\t\t\t}\n\t\t\t}\n\t\t\t]\n\t\t});\t\n\t\t\n\t\n\n\t\t// validate form.\n\t\t$(\"#Save\").click(function () {\n\t\t\tvar validationResult ="+
			" function (isValid) {\n\t\t\t\tif (isValid) {\n\t\t\t\t\t$(\"#addnew_form\").submit();\n\t\t\t\t}\n\t\t\t}\n\t\t\t$(\'#addnew_form\').jqxValidator(\'validate\', "+
			"validationResult);\n\t\t});\n\t\t";









	public static  String changecost_js="$(\"#edit_cost_Input\").jqxNumberInput({ width: \'200\', height: \'25px\',decimalDigits: 2,decimal: 0 , min: 0,inputMode: "+
			"\'simple\', spinButtons: true,spinMode: \'simple\',spinButtonsStep:1,groupSize: 3, groupSeparator: \',\',symbol: \'Rs \'});\n\t\t\n\t\t\n\t\t\n\t\t$(\"#changecost_popupWindow\").jqxWindow({\n width: 340,height:150, resizable: false, theme: theme, isModal: true, autoOpen: false,"+
			" cancelButton: $(\"#Cancel\"), modalOpacity: 0.20,position:\'center\' \n });\n\t\t\t\n\t\t\t\n\t\t$(\"#cost_save\").jqxButton({ theme: theme,width:"+
			"  \'100\'});\t\n\t\t\n\t\t\n\t\t$(\'#change_cost_form\').jqxValidator({\n\t\t\trules: [\n\t\t\t\n\t\t\t{ input: \'#edit_cost_Input\', message: \'Cost of product must be"+
			"  more the 0 !\', action: \'keyup, focus\', rule: function (input, commit) {\n\t\t\t\tif (input.val()>0) {\n\t\t\t\t\treturn true;\n\t\t\t\t}\n\t\t\t\t\treturn"+
			"  false;\n\t\t\t\t}\n\t\t\t}\n\t\t\t]\n\t\t});\t\n\t\t\n\t\t\n\t\t// validate form.\n\t\t$(\"#cost_save\").click(function () {\n\t\t\tvar validationResult = function (isValid)"+
			"  {\n\t\t\t\tif (isValid) {\n\t\t\t\t\t$(\"#change_cost_form\").submit();\n\t\t\t\t}\n\t\t\t}\n\t\t\t$(\'#change_cost_form\').jqxValidator(\'validate\',"+
			"  validationResult);\n\t\t});";




	public static  String changeQuantity_js=" $(\"#edit_quantity_Input\").jqxNumberInput({ width: \'200\', height: \'25px\',decimalDigits: 0,decimal: 0 , min: 0,inputMode: \'simple\',"+
			" spinButtons: true });\n\t\t$(\"#changeQuantity_popupWindow\").jqxWindow({\n width: 340,height:150, resizable: false, theme: theme, isModal: true, autoOpen: false,"+
			" cancelButton: $(\"#Cancel\"), modalOpacity: 0.20,position:\'center\' \n });\n\t\t\t\n\t\t\t\n\t\t$(\"#quantity_save\").jqxButton({ theme: theme,width:"+
			"  \'100\'});\t\n\t\t\t\n\t\t\t$(\'#change_quantity_form\').jqxValidator({\n\t\t\trules: [\n\t\t\t\n\t\t\t{ input: \'#edit_quantity_Input\', message: \'Quantity of product"+
			"  must be more the 0 !\', action: \'keyup, focus\', rule: function (input, commit) {\n\t\t\t\tif (input.val()>0) {\n\t\t\t\t\treturn true;\n\t\t\t\t}\n\t\t\t\t\treturn"+
			"  false;\n\t\t\t\t}\n\t\t\t}\n\t\t\t]\n\t\t});\t\n\t\t\n\t\t// validate form.\n\t\t$(\"#quantity_save\").click(function () {\n\t\t\tvar validationResult = function (isValid) "+
			" {\n\t\t\t\tif (isValid) {\n\t\t\t\t\t$(\"#change_quantity_form\").submit();\n\t\t\t\t}\n\t\t\t}\n\t\t\t$(\'#change_quantity_form\').jqxValidator(\'validate\', "+
			" validationResult);\n\t\t});";


	public static  String edit_cost_btn_js="function edit_cost_btn_click(engine_id,cost_value){\n\t$(\"#cost_engine_id\").val(engine_id);\n\t$(\"#edit_cost_Input\").val(cost_value);\n\t$(\"#changecost_popu"+
			"pWindow\").jqxWindow(\'open\');\n}";


	public static  String edit_quantity_btn_js="function edit_quantity_btn_click(engine_id,quantity_value){\n\t$(\"#quantity_engine_id\").val(engine_id);\n\t$(\"#edit_quantity_Input\").val(quantity_value);\n\t "+
			"$(\"#changeQuantity_popupWindow\").jqxWindow(\'open\');\t\n}";


	public static  String delete_btn_js="function delete_btn_click(engine_id){\n\t$(\"#delete_engine_id\").val(engine_id);\n\t\n\t "+
			"$(\"#deleteproduct_popupWindow\").jqxWindow(\'open\');\t\n}";





	public static  String addnew_popupWindow="<div id=\"addnew_popupWindow\">\n <div>Add New Engine</div>\n <div style=\"overflow: "+
						  "hidden;\">\n\t\t\t<form class=\"form\" id=\"addnew_form\" target=\"form-iframe\" method=\"post\" "+
						  "action=\"addProduct\" style=\"font-size: 13px; font-family: Verdana; width: 650px;\">\n \n\t"+
						  " <table class=\"register-table\">\n "+"<tr>\n <td>Engine Id:</td>\n <td><input name=\"engine_id\""+
						  " type=\"text\" name=\"cubic_capacity\" id=\"engine_id_Input\" class=\"text-input\" /></td>\n </tr>\n\t\t\t\t\t<tr>\n "+
						  "<td>Fuel Type:</td>\n <td><div style=\'margin-top: 3px;\' name=\"fuel_type\" id=\'fuel_type_List\' ></div></td>\n"+
						  " </tr>\n <tr>\n <td>Cubic Capacity:</td>\n <td><div style=\'margin-top: 3px;\' name=\"cubic_capacity\" "+
						  "id=\'cubic_capacity_Input\' placeholder=\"Cubic Capacity\" ></div></td>\n </tr>\n <tr>\n <td>No Of Cylinder:</td>\n "+
						  "<td><div style=\'margin-top: 3px;\' name=\"no_of_cylinder\" id=\'no_of_cylinder_Input\' placeholder=\"No Of Cylinder\" >"+
						  "</div></td>\n </tr>\n \n <tr>\n <td>Mounting Type:</td>\n <td><div style=\'margin-top: 3px;\' name=\"mounting_type\" "+
						  "id=\'mounting_type_List\' ></div></td>\n </tr>\n <tr>\n <td>Engine Cost:</td>\n <td><div style=\'margin-top: 3px;\' "+
						  "name=\"engine_cost\" id=\'engine_cost_Input\' placeholder=\"Engine Cost\" ></div></td>\n </tr>\n <tr>\n <td>Available Quantity:</td>"+
						  "\n <td><div style=\'margin-top: 3px;\' name=\"quantity\" id=\'quantity_Input\' placeholder=\"Quantity\" ></div></td>\n </tr>\n \n <tr>\n"+
						  " <td ></td>\n <td style=\"padding-top: 10px;\" align=\"left\"><input style=\"margin-right: 10px;\" type=\"button\" id=\"Save\" value=\"Save\" />"+
						  "<input id=\"Cancel\" class=\"cancel\" type=\"button\" value=\"Cancel\" /></td>\n </tr>\n </table>\n \n </form>\n \n </div>\n </div>";
						  
						  
						  
						  



	public static  String changecost_popupWindow=" <div id=\"changecost_popupWindow\">\n <div>Change Cost</div>\n <div"+
			  " style=\"overflow: hidden;\">\n <form class=\"form\" id=\"change_cost_form\" "+
			  " method=\"post\" action=\"updateCost\" style=\"font-size: 13px; font-family: "+
			  " Verdana; width: 650px;\">\n \n\t\t\t<input type=\"hidden\" "+
			  " name=\"cost_engine_id\" id=\"cost_engine_id\" value=\"\" />\n\t <table "+
			  " class=\"register-table\">\n \n \n <tr>\n <td>Cost of Product:</td>\n <td><div "+
			  " style=\'margin-top: 3px;\' name=\"engine_cost\" id=\'edit_cost_Input\' "+
			  " placeholder=\"Cost of Product\" ></div></td>\n </tr>\n \n <tr>\n <td ></td>\n <td "+
			  " style=\"padding-top: 10px;\" align=\"center\"><input style=\"margin-right: 5px;\" "+
			  " type=\"button\" id=\"cost_save\" value=\"Save\" /><input id=\"Cancel\" "+
			  " class=\"cancel\" type=\"button\" value=\"Cancel\" /></td>\n </tr>\n </table>\n \n "+
			  " </form>\n </div>\n </div> ";
			  
			  
			  
			  
	public static  String changeQuantity_popupWindow="<div id=\"changeQuantity_popupWindow\">\n "+
			 " <div>Change Quanitity</div>\n <div style=\"overflow: hidden;\">\n <form "+
			 "  class=\"form\" id=\"change_quantity_form\" method=\"post\" "+
			 "  action=\"updateQuantity\" style=\"font-size: 13px; font-family: Verdana; width: "+
			 "  650px;\">\n \n\t\t\t<input type=\"hidden\" name=\"quantity_engine_id\" "+
			 "  id=\"quantity_engine_id\" value=\"\" />\n\t <table class=\"register-table\">\n \n "+
			 "  \n <tr>\n <td>New Quantity:</td>\n <td><div style=\'margin-top: 3px;\' "+
			 "  name=\"quantity\" id=\'edit_quantity_Input\' placeholder=\"Quantity\" "+
			 "  ></div></td>\n </tr>\n \n <tr>\n <td></td>\n <td style=\"padding-top: 10px; "+
			 "  width:200px;\" align=\"right\" ><input style=\"margin-right: 5px;\" "+
			 "  type=\"button\" id=\"quantity_save\" value=\"Save\" /><input id=\"Cancel\" "+
			 "  class=\"cancel\" type=\"button\" value=\"Cancel\" /></td>\n </tr>\n </table>\n \n "+
			 "  </form>\n </div>\n </div> ";






	public static  String deleteproduct_popupWindow=" <div id=\"deleteproduct_popupWindow\">\n <div>Delete Product</div>\n "+
			 " <div style=\"overflow: hidden;\">\n <form class=\"form\" id=\"delete_product_form\"  "+
			 " method=\"post\" action=\"productDelete\" style=\"font-size: 13px; font-family: "+
			 "  Verdana; width: 650px;\">\n \n\t\t\t<input type=\"hidden\" name=\"delete_engine_id\"  "+
			 " id=\"delete_engine_id\" value=\"\" />\n\t\t\t<p >Are You Sure?</p>\n\t <input style=\"margin-left:  "+
			 " 50px; margin-right: 10px;\" type=\"button\" id=\"delete\" value=\"Delete\" /><input id=\"Cancel\"  "+
			 " class=\"cancel\" type=\"button\" value=\"Cancel\" />\n \n </form>\n </div>\n </div> ";












}
